package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    /** Zavolano po zmacknuti hrace */
    public void editPlayerStat(View view){
        startActivity(new Intent(getApplicationContext(), MainActivity2.class));
    }

    /**Zavolano po zmacknuti tlacitka pro zobrazeni statistik */
    public void showStats(View view){
        startActivity(new Intent(getApplicationContext(), BasicStat.class));
    }
}
