package cz.example.petr.BasketballStats;

/**
 * Created by Petr on 04.10.2016.
 */

public class Player {
    private int _ID_Player;
    private String _Player_name;
    private String _Player_surname;
    private String _Player_nickname;
    private int _Player_number;
    private int _Player_team;
    private int _Player_button;
    private int _Player_ingame;

    public Player(){

    }

    public Player(int id, String playername, String playersurname, String playernickname, int playernumber, int playerteam, int playerbutton, int playeringame){
        this._ID_Player = id;
        this._Player_name = playername;
        this._Player_surname = playersurname;
        this._Player_nickname = playernickname;
        this._Player_number = playernumber;
        this._Player_team = playerteam;
        this._Player_button = playerbutton;
        this._Player_ingame = playeringame;
    }

    public Player(String playername, String playersurname, String playernickname, int playernumber, int playerteam, int playerbutton, int playeringame){
        this._Player_name = playername;
        this._Player_surname = playersurname;
        this._Player_nickname = playernickname;
        this._Player_number = playernumber;
        this._Player_team = playerteam;
        this._Player_button = playerbutton;
        this._Player_ingame = playeringame;
    }

    public void setID(int id){
        this._ID_Player = id;
    }

    public int getID(){
        return this._ID_Player;
    }

    public void setPlayerName(String playername){
        this._Player_name = playername;
    }

    public String getPlayerName(){
        return this._Player_name;
    }

    public void setPlayerSurname(String playersurname){
        this._Player_surname = playersurname;
    }

    public String getPlayerSurname(){
        return this._Player_surname;
    }

    public void setPlayerNickname(String playernickname){
        this._Player_nickname = playernickname;
    }

    public String getPlayerNickname(){
        return this._Player_nickname;
    }

    public void setPlayerNumber(int playernumber){
        this._Player_number = playernumber;
    }

    public int getPlayerNumber(){
        return this._Player_number;
    }

    public void setPlayerTeam(int playerteam){
        this._Player_team = playerteam;
    }

    public int getPlayerTeam(){
        return this._Player_team;
    }

    public void setPlayerButton(int playerbutton){
        this._Player_button = playerbutton;
    }

    public int getPlayerButton(){
        return this._Player_button;
    }

    public void setPlayerIngame(int playeringame){
        this._Player_ingame = playeringame;
    }

    public int getPlayerIngame(){
        return this._Player_ingame;
    }
}
