package cz.example.petr.BasketballStats;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class PlayersInGameActivity extends AppCompatActivity {

    private static int id_team, game;
    private static String action;
    Button p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15;
    boolean edit = false;
    String teamkey, gamekey;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference playerDatabaseReference;
    private DatabaseReference actionDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_in_game);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            edit = extras.getBoolean("Edit");
            id_team = extras.getInt("Team_ID");
            if(edit == false){
                action = extras.getString("Action");
                game = extras.getInt("Game_ID");
                teamkey = extras.getString("teamkey");
                gamekey = extras.getString("gamekey");
                UID = extras.getString("UserUID");
                Log.d("Reading ID_Team:", "Reading..." + id_team);
                Log.d("Reading Action:", "Reading..." + action);
            }
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na players
        playerDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("players");
        playerDatabaseReference.keepSynced(true);
        //reference na actions
        actionDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games").child(gamekey).child("actions");
        actionDatabaseReference.keepSynced(true);

        p1 = (Button) findViewById(R.id.player1);
        p2 = (Button) findViewById(R.id.player2);
        p3 = (Button) findViewById(R.id.player3);
        p4 = (Button) findViewById(R.id.player4);
        p5 = (Button) findViewById(R.id.player5);
        p6 = (Button) findViewById(R.id.player6);
        p7 = (Button) findViewById(R.id.player7);
        p8 = (Button) findViewById(R.id.player8);
        p9 = (Button) findViewById(R.id.player9);
        p10 = (Button) findViewById(R.id.player10);
        p11 = (Button) findViewById(R.id.player11);
        p12 = (Button) findViewById(R.id.player12);
        p13 = (Button) findViewById(R.id.player13);
        p14 = (Button) findViewById(R.id.player14);
        p15 = (Button) findViewById(R.id.player15);

        showPlayers();
    }

    public void showPlayers(){

        /**pro update dat v realnem case...data se zobrazuji v listu*/
        playerDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                p1.setText("-");
                p1.setTextColor(Color.parseColor("#BFBFBF"));
                p2.setText("-");
                p2.setTextColor(Color.parseColor("#BFBFBF"));
                p3.setText("-");
                p3.setTextColor(Color.parseColor("#BFBFBF"));
                p4.setText("-");
                p4.setTextColor(Color.parseColor("#BFBFBF"));
                p5.setText("-");
                p5.setTextColor(Color.parseColor("#BFBFBF"));
                p6.setText("-");
                p6.setTextColor(Color.parseColor("#BFBFBF"));
                p7.setText("-");
                p7.setTextColor(Color.parseColor("#BFBFBF"));
                p8.setText("-");
                p8.setTextColor(Color.parseColor("#BFBFBF"));
                p9.setText("-");
                p9.setTextColor(Color.parseColor("#BFBFBF"));
                p10.setText("-");
                p10.setTextColor(Color.parseColor("#BFBFBF"));
                p11.setText("-");
                p11.setTextColor(Color.parseColor("#BFBFBF"));
                p12.setText("-");
                p12.setTextColor(Color.parseColor("#BFBFBF"));
                p13.setText("-");
                p13.setTextColor(Color.parseColor("#BFBFBF"));
                p14.setText("-");
                p14.setTextColor(Color.parseColor("#BFBFBF"));
                p15.setText("-");
                p15.setTextColor(Color.parseColor("#BFBFBF"));
                for (DataSnapshot playerDataSnapshot : dataSnapshot.getChildren()){
                    final Player player = playerDataSnapshot.getValue(Player.class);
                    final String playerkey = playerDataSnapshot.getKey();
                    if (player.getPlayerButton() == 1){
                        p1.setTextColor(Color.parseColor("#ffffff"));
                        p1.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p1.setTextColor(Color.parseColor("#06BF25"));
                            p1.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                }
                            });
                        }
                        else{
                            p1.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 2){
                        p2.setTextColor(Color.parseColor("#ffffff"));
                        p2.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p2.setTextColor(Color.parseColor("#06BF25"));
                            p2.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                }
                            });
                        }
                        else{
                            p2.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 3){
                        p3.setTextColor(Color.parseColor("#ffffff"));
                        p3.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p3.setTextColor(Color.parseColor("#06BF25"));
                            p3.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p3.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 4){
                        p4.setTextColor(Color.parseColor("#ffffff"));
                        p4.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p4.setTextColor(Color.parseColor("#06BF25"));
                            p4.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p4.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 5){
                        p5.setTextColor(Color.parseColor("#ffffff"));
                        p5.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p5.setTextColor(Color.parseColor("#06BF25"));
                            p5.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p5.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 6){
                        p6.setTextColor(Color.parseColor("#ffffff"));
                        p6.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p6.setTextColor(Color.parseColor("#06BF25"));
                            p6.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p6.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 7){
                        p7.setTextColor(Color.parseColor("#ffffff"));
                        p7.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p7.setTextColor(Color.parseColor("#06BF25"));p7.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });

                        }
                        else{
                            p7.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 8){
                        p8.setTextColor(Color.parseColor("#ffffff"));
                        p8.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p8.setTextColor(Color.parseColor("#06BF25"));
                            p8.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p8.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 9){
                        p9.setTextColor(Color.parseColor("#ffffff"));
                        p9.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p9.setTextColor(Color.parseColor("#06BF25"));
                            p9.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p9.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 10){
                        p10.setTextColor(Color.parseColor("#ffffff"));
                        p10.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p10.setTextColor(Color.parseColor("#06BF25"));
                            p10.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p10.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 11){
                        p11.setTextColor(Color.parseColor("#ffffff"));
                        p11.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p11.setTextColor(Color.parseColor("#06BF25"));
                            p11.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p11.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 12){
                        p12.setTextColor(Color.parseColor("#ffffff"));
                        p12.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p12.setTextColor(Color.parseColor("#06BF25"));
                            p12.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p12.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 13){
                        p13.setTextColor(Color.parseColor("#ffffff"));
                        p13.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p13.setTextColor(Color.parseColor("#06BF25"));
                            p13.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p13.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 14){
                        p14.setTextColor(Color.parseColor("#ffffff"));
                        p14.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p14.setTextColor(Color.parseColor("#06BF25"));
                            p14.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p14.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 15){
                        p15.setTextColor(Color.parseColor("#ffffff"));
                        p15.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        if (player.getPlayerIngame() == 1){
                            p15.setTextColor(Color.parseColor("#06BF25"));
                            p15.setOnClickListener(new View.OnClickListener(){
                                @Override
                                public void onClick(View v){
                                    addAction(player.getPlayerNickname());
                                    finish();
                                }
                            });
                        }
                        else{
                            p15.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*DBHandler db = new DBHandler(this, null, null, 1);
        int b = 1;  //button, nak kterem je
        final Player player1 = db.findPlayer(id_team, b);
        if(player1 != null){
            if (player1.getPlayerIngame() == 1){
                p1.setTextColor(Color.parseColor("#06BF25"));
                p1.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player1.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player1.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p1.setTextColor(Color.parseColor("#ffffff"));
            }
            p1.setText(String.valueOf(player1.getPlayerNumber()) + "\n\n" + player1.getPlayerNickname());
        }
        else{
            p1.setText("-");
        }
        b = 2;
        final Player player2 = db.findPlayer(id_team, b);
        if(player2 != null) {
            if (player2.getPlayerIngame() == 1){
                p2.setTextColor(Color.parseColor("#06BF25"));
                p2.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player2.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player2.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p2.setTextColor(Color.parseColor("#ffffff"));
            }
            p2.setText(String.valueOf(player2.getPlayerNumber()) + "\n\n" + player2.getPlayerNickname());
        }
        else{
            p2.setText("-");
        }
        b = 3;
        final Player player3 = db.findPlayer(id_team, b);
        if(player3 != null) {
            if (player3.getPlayerIngame() == 1){
                p3.setTextColor(Color.parseColor("#06BF25"));
                p3.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player3.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player3.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p3.setTextColor(Color.parseColor("#ffffff"));
            }
            p3.setText(String.valueOf(player3.getPlayerNumber()) + "\n\n" + player3.getPlayerNickname());
        }
        else{
            p3.setText("-");
        }
        b = 4;
        final Player player4 = db.findPlayer(id_team, b);
        if(player4 != null) {
            if (player4.getPlayerIngame() == 1){
                p4.setTextColor(Color.parseColor("#06BF25"));
                p4.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player4.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player4.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p4.setTextColor(Color.parseColor("#ffffff"));
            }
            p4.setText(String.valueOf(player4.getPlayerNumber()) + "\n\n" + player4.getPlayerNickname());
        }
        else{
            p4.setText("-");
        }
        b = 5;
        final Player player5 = db.findPlayer(id_team, b);
        if(player5 != null) {
            if (player5.getPlayerIngame() == 1){
                p5.setTextColor(Color.parseColor("#06BF25"));
                p5.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player5.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player5.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p5.setTextColor(Color.parseColor("#ffffff"));
            }
            p5.setText(String.valueOf(player5.getPlayerNumber()) + "\n\n" + player5.getPlayerNickname());
        }
        else{
            p5.setText("-");
        }
        b = 6;
        final Player player6 = db.findPlayer(id_team, b);
        if(player6 != null) {
            if (player6.getPlayerIngame() == 1){
                p6.setTextColor(Color.parseColor("#06BF25"));
                p6.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player6.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player6.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p6.setTextColor(Color.parseColor("#ffffff"));
            }
            p6.setText(String.valueOf(player6.getPlayerNumber()) + "\n\n" + player6.getPlayerNickname());
        }
        else{
            p6.setText("-");
        }
        b = 7;
        final Player player7 = db.findPlayer(id_team, b);
        if(player7 != null) {
            if (player7.getPlayerIngame() == 1){
                p7.setTextColor(Color.parseColor("#06BF25"));
                p7.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player7.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player7.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p7.setTextColor(Color.parseColor("#ffffff"));
            }
            p7.setText(String.valueOf(player7.getPlayerNumber()) + "\n\n" + player7.getPlayerNickname());
        }
        else{
            p7.setText("-");
        }
        b = 8;
        final Player player8 = db.findPlayer(id_team, b);
        if(player8 != null) {
            if (player8.getPlayerIngame() == 1){
                p8.setTextColor(Color.parseColor("#06BF25"));
                p8.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player8.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player8.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p8.setTextColor(Color.parseColor("#ffffff"));
            }
            p8.setText(String.valueOf(player8.getPlayerNumber()) + "\n\n" + player8.getPlayerNickname());
        }
        else{
            p8.setText("-");
        }
        b = 9;
        final Player player9 = db.findPlayer(id_team, b);
        if(player9 != null) {
            if (player9.getPlayerIngame() == 1){
                p9.setTextColor(Color.parseColor("#06BF25"));
                p9.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player9.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player9.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p9.setTextColor(Color.parseColor("#ffffff"));
            }
            p9.setText(String.valueOf(player9.getPlayerNumber()) + "\n\n" + player9.getPlayerNickname());
        }
        else{
            p9.setText("-");
        }
        b = 10;
        final Player player10 = db.findPlayer(id_team, b);
        if(player10 != null) {
            if (player10.getPlayerIngame() == 1){
                p10.setTextColor(Color.parseColor("#06BF25"));
                p10.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player10.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player10.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p10.setTextColor(Color.parseColor("#ffffff"));
            }
            p10.setText(String.valueOf(player10.getPlayerNumber()) + "\n\n" + player10.getPlayerNickname());
        }
        else{
            p10.setText("-");
        }
        b = 11;
        final Player player11 = db.findPlayer(id_team, b);
        if(player11 != null) {
            if (player11.getPlayerIngame() == 1){
                p11.setTextColor(Color.parseColor("#06BF25"));
                p11.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player11.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player11.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p11.setTextColor(Color.parseColor("#ffffff"));
            }
            p11.setText(String.valueOf(player11.getPlayerNumber()) + "\n\n" + player11.getPlayerNickname());
        }
        else{
            p11.setText("-");
        }
        b = 12;
        final Player player12 = db.findPlayer(id_team, b);
        if(player12 != null) {
            if (player12.getPlayerIngame() == 1){
                p12.setTextColor(Color.parseColor("#06BF25"));
                p12.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player12.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player12.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p12.setTextColor(Color.parseColor("#ffffff"));
            }
            p12.setText(String.valueOf(player12.getPlayerNumber()) + "\n\n" + player12.getPlayerNickname());
        }
        else{
            p12.setText("-");
        }
        b = 13;
        final Player player13 = db.findPlayer(id_team, b);
        if(player13 != null) {
            if (player13.getPlayerIngame() == 1){
                p13.setTextColor(Color.parseColor("#06BF25"));
                p13.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player13.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player13.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p13.setTextColor(Color.parseColor("#ffffff"));
            }
            p13.setText(String.valueOf(player13.getPlayerNumber()) + "\n\n" + player13.getPlayerNickname());
        }
        else{
            p13.setText("-");
        }
        b = 14;
        final Player player14 = db.findPlayer(id_team, b);
        if(player14 != null) {
            if (player14.getPlayerIngame() == 1){
                p14.setTextColor(Color.parseColor("#06BF25"));
                p14.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player14.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player14.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p14.setTextColor(Color.parseColor("#ffffff"));
            }
            p14.setText(String.valueOf(player14.getPlayerNumber()) + "\n\n" + player14.getPlayerNickname());
        }
        else{
            p14.setText("-");
        }
        b = 15;
        final Player player15 = db.findPlayer(id_team, b);
        if(player15 != null) {
            if (player15.getPlayerIngame() == 1){
                p15.setTextColor(Color.parseColor("#06BF25"));
                p15.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        if(edit == true){
                            Intent intent = new Intent();
                            intent.putExtra("Player", player15.getPlayerNickname());
                            setResult(RESULT_OK, intent);
                        }
                        else{
                            addAction(player15.getPlayerNickname());
                        }
                        finish();
                    }
                });
            }
            else {
                p15.setTextColor(Color.parseColor("#ffffff"));
            }
            p15.setText(String.valueOf(player15.getPlayerNumber()) + "\n\n" + player15.getPlayerNickname());
        }
        else{
            p15.setText("-");
        }*/
    }

    public void addAction(String player_nickname){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            Actions action = new Actions(extras.getString("Action"), player_nickname, extras.getLong("Time"), game);
            Log.d("Reading Action:", "Reading..." + extras.getString("Action"));
            Log.d("Reading PlayerID:", "Reading..." + player_nickname);
            Log.d("Reading Time:", "Reading..." + extras.getLong("Time"));
            Log.d("Reading Game:", "Reading..." + game);
            final String actionId = actionDatabaseReference.push().getKey();
            actionDatabaseReference.child(actionId).setValue(action);
            Intent intent = new Intent();
            intent.putExtra("actionkey", actionId);
            intent.putExtra("action", 1);
            setResult(RESULT_OK, intent);
            finish();
            Log.d("Finishing:", "...");
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }
    }
}
