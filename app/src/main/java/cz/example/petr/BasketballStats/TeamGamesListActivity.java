package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeamGamesListActivity extends AppCompatActivity {

    private List<Game> gameList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GameAdapter gAdapter;

    private static int id_team;
    String teamkey;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference gameDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_games_list);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        gAdapter = new GameAdapter(gameList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(gAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Game game = gameList.get(position);
                Intent i = new Intent(getApplicationContext(), GameDetailActivity.class);
                i.putExtra("gamekey", game.getID());
                i.putExtra("teamkey", teamkey);
                i.putExtra("UserUID", UID);
                startActivity(i);
                Toast.makeText(getApplicationContext(), game.getTeamOne() + "-" +  game.getTeamTwo() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id_team = extras.getInt("Team_ID");
            teamkey = extras.getString("teamkey");
            UID = extras.getString("UserUID");
            Log.d("Reading Teams:", "SHOW_GAMESReading..." + id_team);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }


        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na games
        gameDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games");
        gameDatabaseReference.keepSynced(true);

        /**pro update dat v realnem case...data se zobrazuji v listu*/
        gameDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gameList.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Game game = noteDataSnapshot.getValue(Game.class);
                    Log.d("Game: ", game.getTeamOne() + " vs " + game.getTeamTwo());
                    gameList.add(game);
                    gameDatabaseReference.child(noteDataSnapshot.getKey()).child("id").setValue(noteDataSnapshot.getKey());
                }
                showGames();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    public void showGames(){
        gAdapter.notifyDataSetChanged();
        Log.d("Pocet: ", String .valueOf(gAdapter.getItemCount()));

        /*DBHandler db = new DBHandler(this, null, null, 1);
        Team team = db.findTeam(id_team);
        List<Game> games = db.getAllTeamGames(team.getTeamName());
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scroll = new ScrollView(this);
        Log.d("DB:", "Snazim s vypsat teamy");
        TableLayout tableLayout = (TableLayout) findViewById(R.id.games_table);
        //scroll.addView(tableLayout);
        for(Game game : games){
            TableRow tableRow1 = new TableRow(this);
            tableRow1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            TextView tw1 = new TextView(this);
            tw1.setText("12.12.2016");
            tw1.setTextSize(20);
            tw1.setGravity(Gravity.CENTER);
            tableRow1.setGravity(Gravity.CENTER);
            tableRow1.addView(tw1);
            tableLayout.addView(tableRow1, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            TableRow tableRow2 = new TableRow(this);
            tableRow2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            ImageView img1 = new  ImageView(this);
            img1.setImageResource(R.drawable.trophy);
            img1.setPadding(0,0,0,0);
            TableRow.LayoutParams parms = new TableRow.LayoutParams(0,60);
            img1.setLayoutParams(parms);
            tableRow2.addView(img1);
            TextView tw2 = new TextView(this);
            tw2.setTextColor(Color.parseColor("#4B88A2"));
            tw2.setTextSize(20);
            tw2.setText(game.getTeamOne() + " vs " + game.getTeamTwo());
            final int id = game.getID();
            tw2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), GameDetailActivity.class);
                    i.putExtra("Game_ID", id);
                    startActivity(i);
                }
            });
            tableRow2.addView(tw2);
            tableRow2.setGravity(Gravity.LEFT);
            tableLayout.addView(tableRow2, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            /*final TextView tw1 = new TextView(this);
            final ImageView img1 = new  ImageView(this);
            img1.setImageResource(R.drawable.trophy);
            linearLayout.addView(img1);
            tw1.setTextColor(Color.parseColor("#4B88A2"));
            tw1.setTextSize(25);
            tw1.setText(game.getTeamOne() + " vs " + game.getTeamTwo());
            linearLayout.addView(tw1);*/
            /*String log = "ID: " + game.getID() + ", TeamOne: " + game.getTeamOne() + " TeamTwo: " + game.getTeamTwo();
            Log.d("Team::", log);
        }*/
        //this.setContentView(scroll, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
    }
}
