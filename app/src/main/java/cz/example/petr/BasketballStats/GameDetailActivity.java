package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class GameDetailActivity extends AppCompatActivity {

    String gamekey, teamkey;
    TextView showteams;
    Button gamedetail;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_detail);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            gamekey = extras.getString("gamekey");
            teamkey = extras.getString("teamkey");
            UID = extras.getString("UserUID");
            Log.d("Reading Game:", "SHOW_GAMEReading..." + gamekey);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        showteams = (TextView) findViewById(R.id.show_teams);
        gamedetail = (Button) findViewById(R.id.show_timeline);

        gamedetail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), ShowTimelineActivity.class);
                i.putExtra("gamekey", gamekey);
                i.putExtra("teamkey", teamkey);
                i.putExtra("UserUID", UID);
                startActivity(i);
            }
        });

        showTeam();
    }

    protected void showTeam(){
        /*DBHandler dbHandler = new DBHandler(this, null, null, 1);
        Game game = dbHandler.findGame(id_game);
        showteams.setText(game.getTeamOne() + " vs " + game.getTeamTwo());
        List<Actions> actiones = dbHandler.getAllGameActions(id_game);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ScrollView scroll = new ScrollView(this);
        Log.d("DB:", "Snazim s vypsat teamy");
        scroll.addView(linearLayout);
        for(Actions action : actiones){
            final TextView tw1 = new TextView(this);

            long actionTime, min, sec;
            actionTime = action.getTime();
            min = (actionTime/(1000*60));
            sec = ((actionTime/1000)-min*60);
            tw1.setText(String.format("%02d",min) + ":" + String.format("%02d",sec) + " " + action.getPlayer() + " " + action.getAction());
            linearLayout.addView(tw1);
        }
        this.setContentView(scroll, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));*/
    }
}
