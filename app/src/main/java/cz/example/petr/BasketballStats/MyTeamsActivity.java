package cz.example.petr.BasketballStats;
/**Trida pro praci s tymy*/

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MyTeamsActivity extends AppCompatActivity {

    private static final int ADD_OK = 1;
    Button addteam, deleteteam, t1, t2, t3, t4, t5, t6, signOutButton;
    private static boolean delete;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private String mUsername, UID;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_teams);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            UID = extras.getString("UserUID");
            Log.d("Reading UID:", "Reading..." + UID);
        }
        else {
            Log.d("Reading Button:", "Nothing to read...");
        }

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);

        addteam = (Button) findViewById(R.id.add_team);
        deleteteam = (Button) findViewById(R.id.delete_team);
        t1 = (Button) findViewById(R.id.team1);
        t2 = (Button) findViewById(R.id.team2);
        t3 = (Button) findViewById(R.id.team3);
        t4 = (Button) findViewById(R.id.team4);
        t5 = (Button) findViewById(R.id.team5);
        t6 = (Button) findViewById(R.id.team6);
        signOutButton = (Button)findViewById(R.id.sign_out_button);

        showTeams();

        addteam.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddTeamsActivity.class);
                delete = false;
                i.putExtra("Delete", delete);
                i.putExtra("UserUID", UID);
                delete = false;
                i.putExtra("Delete", delete);
                startActivityForResult(i, ADD_OK);
            }
        });

        deleteteam.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddTeamsActivity.class);
                delete = true;
                i.putExtra("Delete", delete);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });

        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == ADD_OK){
            showTeams();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showTeams(){
        /**pro update dat v realnem case...data se zobrazuji v listu*/
        teamDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    final Team team = noteDataSnapshot.getValue(Team.class);
                    Log.d("----TeamUser: ", team.getTeamUser() + "UID: " + UID);
                    final int teamid = team.getID();
                    final String teamkey = noteDataSnapshot.getKey();
                    if(team.getTeamButton() == 1){
                        t1.setVisibility(View.VISIBLE);
                        t1.setText(team.getTeamName());
                        t1.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if(team.getTeamButton() == 2){
                        t2.setVisibility(View.VISIBLE);
                        t2.setText(team.getTeamName());
                        t2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if(team.getTeamButton() == 3){
                        t3.setVisibility(View.VISIBLE);
                        t3.setText(team.getTeamName());
                        t3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if(team.getTeamButton() == 4){
                        t4.setVisibility(View.VISIBLE);
                        t4.setText(team.getTeamName());
                        t4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if(team.getTeamButton() == 5){
                        t5.setVisibility(View.VISIBLE);
                        t5.setText(team.getTeamName());
                        t5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if(team.getTeamButton() == 6){
                        t6.setVisibility(View.VISIBLE);
                        t6.setText(team.getTeamName());
                        t6.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getApplicationContext(), ShowSelectedTeamActivity.class);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
