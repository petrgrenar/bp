package cz.example.petr.BasketballStats;
/**Trida pro praci s hraci*/

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class MyPlayersActivity extends AppCompatActivity {

    private static final int ADD_OK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_players);

        showPlayers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == ADD_OK){
            showPlayers();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showPlayers(){
        DBHandler db = new DBHandler(this, null, null, 1);
        List<Player> players = db.getAllPlayers();
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        for(Player player : players){
            final TextView tw1 = new TextView(this);
            tw1.setTextColor(Color.parseColor("#4B88A2"));
            tw1.setTextSize(25);
            tw1.setText(String.valueOf(player.getPlayerName()) + " " + String.valueOf(player.getPlayerSurname()) + " " + String.valueOf(player.getPlayerNumber()));
            linearLayout.addView(tw1);
            String log = "ID: " + player.getID() + ", Name: " + player.getPlayerName() + " Surname: " + player.getPlayerSurname() + " Number: " + player.getPlayerNumber() + " Team: " + player.getPlayerTeam();
            Log.d("Player::", log);
        }
        Button add_player = new Button(this);
        add_player.setText(R.string.add_player);
        linearLayout.addView(add_player);
        this.setContentView(linearLayout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        add_player.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                startActivityForResult(i, ADD_OK);
            }
        });
    }
}
