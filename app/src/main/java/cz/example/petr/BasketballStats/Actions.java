package cz.example.petr.BasketballStats;


/**
 * Created by Petr on 29.10.2016.
 */

public class Actions {
    private int _ID_Action;
    private String _Action;
    private String _Player;
    private long _Time;
    private int _Game;

    public Actions() {

    }

    public Actions(int id, String action, String player, long time, int game){
        this._ID_Action = id;
        this._Action = action;
        this._Player = player;
        this._Time = time;
        this._Game = game;
    }

    public Actions(String action, String player, long time, int game){
        this._Action = action;
        this._Player = player;
        this._Time = time;
        this._Game = game;
    }

    public void setID(int id){
        this._ID_Action = id;
    }

    public int getID(){
        return this._ID_Action;
    }

    public void setAction(String action){
        this._Action = action;
    }

    public String getAction(){
        return this._Action;
    }

    public void setPlayer(String player){
        this._Player = player;
    }

    public String getPlayer(){
        return this._Player;
    }

    public void setTime(long time){
        this._Time = time;
    }

    public long getTime(){
        return this._Time;
    }

    public void set_Game(int game){
        this._Game = game;
    }

    public int get_Game(){
        return this._Game;
    }
}