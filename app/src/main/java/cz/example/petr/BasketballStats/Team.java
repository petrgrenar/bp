package cz.example.petr.BasketballStats;

/**
 * Created by Petr on 03.10.2016.
 * Trida pro jednotlive tymy
 */

public class Team {
    private  int _ID_Team;
    private String _Team_name;
    private int _Team_Button;
    private String _User;

    public Team(){

    }

    public Team(int id, String teamname, int teambutton, String user){
        this._ID_Team = id;
        this._Team_name = teamname;
        this._Team_Button = teambutton;
        this._User = user;
    }

    public Team(String teamname, int teambutton, String user){
        this._Team_name = teamname;
        this._Team_Button = teambutton;
        this._User = user;
    }

    public void setID(int id){
        this._ID_Team = id;
    }

    public int getID(){
        return this._ID_Team;
    }

    public void setTeamName(String teamname){
        this._Team_name = teamname;
    }

    public String getTeamName(){
        return this._Team_name;
    }

    public void setTeamButton(int teambutton){
        this._Team_Button = teambutton;
    }

    public int getTeamButton(){
        return this._Team_Button;
    }

    public void setTeamUser(String user){
        this._User = user;
    }

    public String getTeamUser(){
        return this._User;
    }
}
