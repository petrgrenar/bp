package cz.example.petr.BasketballStats;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Petr on 08.02.2017.
 */

public class ActionAdapter extends RecyclerView.Adapter<ActionAdapter.MyViewHolder>{
    private List<Actions> actionList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView player, action, actiontime;
        public ImageView actionimage;

        public MyViewHolder(View view){
            super(view);
            player = (TextView) view.findViewById(R.id.player);
            action = (TextView) view.findViewById(R.id.action);
            actiontime = (TextView) view.findViewById(R.id.action_time);
            actionimage = (ImageView) view.findViewById(R.id.action_image);
        }
    }

    public ActionAdapter(List<Actions> actionList){
        this.actionList = actionList;
    }

    @Override
    public ActionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.action_list_row, parent, false);
        return new ActionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ActionAdapter.MyViewHolder holder, int position){
        Actions action = actionList.get(position);
        if (action.getAction().equals("2P") || action.getAction().equals("2B")){
            holder.actionimage.setImageResource(R.drawable.add_points);
        }
        else if (action.getAction().equals("2PM") || action.getAction().equals("2BM")){
            holder.actionimage.setImageResource(R.drawable.add_no_points);
        }
        else if (action.getAction().equals("FT") || action.getAction().equals("TH")){
            holder.actionimage.setImageResource(R.drawable.add_point);
        }
        else if (action.getAction().equals("FTM") || action.getAction().equals("THM")){
            holder.actionimage.setImageResource(R.drawable.add_no_point);
        }
        else if (action.getAction().equals("3P") || action.getAction().equals("3B")){
            holder.actionimage.setImageResource(R.drawable.add_three_points);
        }
        else if (action.getAction().equals("3PM") || action.getAction().equals("3BM")){
            holder.actionimage.setImageResource(R.drawable.add_no_three_points);
        }
        holder.player.setText(action.getPlayer());
        holder.action.setText(action.getAction());
        long min, sec;
        min = (action.getTime()/(1000*60));
        sec = ((action.getTime()/1000)-min*60);
        holder.actiontime.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
    }

    @Override
    public int getItemCount(){
        return actionList.size();
    }
}