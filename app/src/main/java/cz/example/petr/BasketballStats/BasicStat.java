package cz.example.petr.BasketballStats;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class BasicStat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_stat);

        /**Nastaveni jednotlivich zobrazovatek*/
        final TextView showPoints;
        showPoints = (TextView) findViewById(R.id.show_points_1);
        final TextView showThreePoints;
        showThreePoints = (TextView) findViewById(R.id.show_three_points_1);
        final TextView showPoint;
        showPoint = (TextView) findViewById(R.id.show_point_1);
        final TextView showOffenceRebound;
        showOffenceRebound = (TextView) findViewById(R.id.show_offence_rebound_1);
        final TextView showDeffenceRebound;
        showDeffenceRebound = (TextView) findViewById(R.id.show_deffence_rebound_1);
        final TextView showObtainedBall;
        showObtainedBall = (TextView) findViewById(R.id.show_obtained_ball_1);
        final TextView showLostBall;
        showLostBall = (TextView) findViewById(R.id.show_lost_ball_1);
        final TextView showObtainedFoul;
        showObtainedFoul = (TextView) findViewById(R.id.show_obtained_foul_1);
        final TextView showPersonalFoul;
        showPersonalFoul = (TextView) findViewById(R.id.show_personal_foul_1);
        final TextView showAllPoints;
        showAllPoints = (TextView) findViewById(R.id.show_all_points_1);

        /**Pro tahani dat ze sharedprefenrences*/
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        /**Nastaveni promennych pro vypis statistik*/
        int P1 = pref.getInt("1P", 0);  //TH
        int P1O =pref.getInt("1PO", 0); //TH - miss
        int P2 = pref.getInt("2P", 0);  //2P
        int P2O = pref.getInt("2PO", 0);  //2P - miss
        int P3 = pref.getInt("3P", 0);  //3P
        int P3O = pref.getInt("3PO", 0);  //3P - miss
        int OR = pref.getInt("OR", 0);  //OR - utocny doskok
        int DR = pref.getInt("DR", 0);  //DR - obrany doskok
        int OB = pref.getInt("OB", 0);  //OD - získaný míč
        int LB = pref.getInt("LB", 0);  //LB - ztracený míč
        int PF = pref.getInt("PF", 0);  //PF - osobní chyba
        int OF = pref.getInt("OF", 0);  //OF - získaný faul
        /**Pocitani bodu a nasledne jejich vypis*/
        int P2all = P2 + P2O;
        int P3all = P3 + P3O;
        int P1all = P1 + P1O;
        int allPoints = P1 + (P2 * 2) + (P3 * 3);
        showPoints.setText(String.valueOf(P2 + "/" + P2all));
        showThreePoints.setText(String.valueOf(P3 + "/" + P3all));
        showPoint.setText(String.valueOf(P1 + "/" + P1all));
        showOffenceRebound.setText(String.valueOf(OR));
        showDeffenceRebound.setText(String.valueOf(DR));
        showObtainedBall.setText(String.valueOf(OB));
        showLostBall.setText(String .valueOf(LB));
        showPersonalFoul.setText(String.valueOf(PF));
        showObtainedFoul.setText(String.valueOf(OF));
        showAllPoints.setText(String.valueOf(allPoints));
    }

    /**Zavolano po zmacknuti tlacitka pro navrat */
    public void getBack(View view){
        finish();
    }
}
