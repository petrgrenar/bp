package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.FirebaseDatabase;

public class TeamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        //Firebase
        //Pro ukládání na disku
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Button settings = (Button) findViewById(R.id.setings);
        settings.setVisibility(View.INVISIBLE);
    }

    /**Zavolano po zmacknuti tlacitka pro obrazeni vsech mych tymu*/
    public void showMyTeams(View view){
        startActivity(new Intent(getApplicationContext(), MyTeamsActivity.class));
    }
}
