package cz.example.petr.BasketballStats;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Petr on 08.02.2017.
 */

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.MyViewHolder>{
    private List<Game> gameList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView hometeam, opponentteam;

        public MyViewHolder(View view){
            super(view);
            hometeam = (TextView) view.findViewById(R.id.home_team);
            opponentteam = (TextView) view.findViewById(R.id.opponent_team);
        }
    }

    public GameAdapter(List<Game> gameList){
        this.gameList = gameList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        Game game = gameList.get(position);
        holder.hometeam.setText(game.getTeamOne());
        holder.opponentteam.setText(game.getTeamTwo());
    }

    @Override
    public int getItemCount(){
        return gameList.size();
    }
}