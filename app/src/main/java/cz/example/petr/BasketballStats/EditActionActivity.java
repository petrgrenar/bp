package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class EditActionActivity extends AppCompatActivity {

    private static int id_game, id_team;
    Button addPoints, addNoPoints, addThreePoints, addNoThreePoints, addFreeThrow, addNoFreeThrow, editButton;
    TextView showMinutes, showSeconds, showColon, showPlayer, showAction, showActionTime;
    ImageButton timePlus, timeMinus;
    long min, sec, actiontime;
    String actionkey, action, player, teamkey, gamekey;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference actionDatabaseReference;

    private static final int SET_OK = 1;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_action);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id_game = extras.getInt("ID");
            id_team = extras.getInt("Team_ID");
            action = extras.getString("action");
            player = extras.getString("player");
            actiontime = extras.getLong("time");
            teamkey = extras.getString("teamkey");
            gamekey = extras.getString("gamekey");
            actionkey = extras.getString("actionkey");
            UID = extras.getString("UserUID");
            Log.d("Reading edited a:", String.valueOf(id_game));
            Log.d("Reading action:", String.valueOf(action));
        }
        else {
            Log.d("Reading actions:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na actions
        actionDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games").child(gamekey).child("actions");
        actionDatabaseReference.keepSynced(true);

        addPoints = (Button) findViewById(R.id.add_points);
        addNoPoints = (Button) findViewById(R.id.add_no_points);
        addThreePoints = (Button) findViewById(R.id.add_three_points);
        addNoThreePoints = (Button) findViewById(R.id.add_no_three_points);
        addFreeThrow = (Button) findViewById(R.id.add_point);
        addNoFreeThrow = (Button) findViewById(R.id.add_no_point);
        showMinutes = (TextView) findViewById(R.id.minutes_view);
        showSeconds = (TextView) findViewById(R.id.seconds_view);
        showColon = (TextView) findViewById(R.id.ColonWiew);
        showPlayer = (TextView) findViewById(R.id.show_player);
        showAction = (TextView) findViewById(R.id.show_action);
        showActionTime = (TextView) findViewById(R.id.show_action_time);
        timePlus = (ImageButton) findViewById(R.id.timer_plus);
        timeMinus = (ImageButton) findViewById(R.id.timer_minus);
        editButton = (Button) findViewById(R.id.edit_button);

        timePlus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                actiontime += 1000;
                setTime(actiontime);
                Log.d("Setting:", "+1...");
            }

        });

        timeMinus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                actiontime -= 1000;
                setTime(actiontime);
                Log.d("Setting:", "-1...");
            }

        });

        addPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_points);
            }
        });

        addNoPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_no_points);
            }
        });

        addThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_three_points);
            }
        });

        addNoThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_no_three_points);
            }
        });

        addFreeThrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_point);
            }
        });

        addNoFreeThrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showAction.setText(R.string.add_no_point);
            }
        });

        editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                editAction();
            }
        });

        showPlayer.setText(player);
        showAction.setText(action);
        min = (actiontime/(1000*60));
        sec = ((actiontime/1000)-min*60);
        showActionTime.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
        showMinutes.setText(""+String.format("%02d",min));
        showSeconds.setText(""+String.format("%02d",sec));
    }

    public void editAction(){
        actionDatabaseReference.child(actionkey).child("action").setValue(showAction.getText().toString());
        actionDatabaseReference.child(actionkey).child("player").setValue(showPlayer.getText().toString());
        actionDatabaseReference.child(actionkey).child("time").setValue(actiontime);
        finish();
    }

    public void setTime(long time){
        min = (time/(1000*60));
        sec = ((time/1000)-min*60);
        showMinutes.setText(""+String.format("%02d",min));
        showSeconds.setText(""+String.format("%02d",sec));
        showActionTime.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == SET_OK){
            if (resultCode == RESULT_OK){
                showPlayer.setText(data.getStringExtra("Player"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
