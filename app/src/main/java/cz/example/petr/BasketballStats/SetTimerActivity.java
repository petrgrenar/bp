package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class SetTimerActivity extends AppCompatActivity {

    TextView setTimer, setMinutes, showColon, setSeconds;
    ImageButton timePlus, timeMinus;
    long time, min, sec;
    CountDownTimer timer;
    boolean timeron;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_timer);

        Typeface timerTypefyce = Typeface.createFromAsset(getAssets(), "segoeuil.ttf");
        setTimer = (TextView) findViewById(R.id.setTimer);
        setMinutes = (TextView) findViewById(R.id.setMinutes);
        showColon = (TextView) findViewById(R.id.showColon);
        setSeconds = (TextView) findViewById(R.id.setSeconds);
        timePlus = (ImageButton) findViewById(R.id.timerPlus);
        timeMinus = (ImageButton) findViewById(R.id.timerMinus);
        setMinutes.setTypeface(timerTypefyce);
        setSeconds.setTypeface(timerTypefyce);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            time = extras.getLong("Time");
            timeron = extras.getBoolean("Timer");
            setTime(time);
            if(timeron == true) {
                startTimer();
            }
            Log.d("Reading:", "Reading..." + time);
        }
        else {
            Log.d("Reading:", "Nothing to read...");
        }

        timePlus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(timeron == true) {
                    timer.cancel();
                    time += 1000;
                    startTimer();
                }
                else{
                    time += 1000;
                    setTime(time);
                }
                Log.d("Setting:", "+1...");
            }

        });

        timeMinus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(timeron == true) {
                    timer.cancel();
                    time -= 1000;
                    startTimer();
                }
                else{
                    time -= 1000;
                    setTime(time);
                }
                Log.d("Setting:", "-1...");
            }

        });

        setMinutes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent();
                intent.putExtra("SetTime", time);
                setResult(RESULT_OK, intent);
                finish();
                Log.d("Finishing:", "...");
            }

        });

        setSeconds.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent();
                intent.putExtra("SetTime", time);
                setResult(RESULT_OK, intent);
                finish();
                Log.d("Finishing:", "...");
            }

        });

        showColon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent();
                intent.putExtra("SetTime", time);
                setResult(RESULT_OK, intent);
                finish();
                Log.d("Finishing:", "...");
            }

        });
    }

    public void setTime(long time){
        min = (time/(1000*60));
        sec = ((time/1000)-min*60);
        setMinutes.setText(""+String.format("%02d",min));
        setSeconds.setText(""+String.format("%02d",sec));
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("SetTime", time);
        setResult(RESULT_OK, intent);
        finish();
        Log.d("Finishing:", "...");
    }

    public void startTimer(){
        timer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished/(1000*60));
                long sec = ((millisUntilFinished/1000)-min*60);
                //showTime.setText(""+String.format("%02d:%02d",min,sec));
                setMinutes.setText(""+String.format("%02d",min));
                setSeconds.setText(""+String.format("%02d",sec));
                if(millisUntilFinished < 1000){
                    setMinutes.setText("00");
                    setSeconds.setText("00");
                }
                time = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                setMinutes.setText("00");
                setSeconds.setText("00");
            }
        };
        timer.start();
    }
}
