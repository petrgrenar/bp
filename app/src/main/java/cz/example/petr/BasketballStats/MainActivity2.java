package cz.example.petr.BasketballStats;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        /**Nastaveni tlacitek a textovich poli pro zadavani statistik */
        final Button addPoint;
        addPoint = (Button) findViewById(R.id.add_point);
        final TextView showPoint;
        showPoint = (TextView) findViewById(R.id.show_point);
        final Button addNoPoint;
        addNoPoint = (Button) findViewById(R.id.add_no_point);
        final TextView showNoPoint;
        showNoPoint = (TextView) findViewById(R.id.show_no_point);
        final Button addPoints;
        addPoints = (Button) findViewById(R.id.add_points);
        final TextView showPoints;
        showPoints = (TextView) findViewById(R.id.show_points);
        final Button addNoPoints;
        addNoPoints = (Button) findViewById(R.id.add_no_points);
        final TextView showNoPoints;
        showNoPoints = (TextView) findViewById(R.id.show_no_points);
        final Button addThreePoints;
        addThreePoints = (Button) findViewById(R.id.add_three_points);
        final TextView showThreePoints;
        showThreePoints = (TextView) findViewById(R.id.show_three_points);
        final Button addNoThreePoints;
        addNoThreePoints = (Button) findViewById(R.id.add_no_three_points);
        final TextView showNoThreePoints;
        showNoThreePoints = (TextView) findViewById(R.id.show_no_three_points);
        final Button addOffenceRebound;
        addOffenceRebound = (Button) findViewById(R.id.add_offence_rebound);
        final TextView showOffenceRebound;
        showOffenceRebound = (TextView) findViewById(R.id.show_offence_rebound);
        final Button addDeffenceRebound;
        addDeffenceRebound = (Button) findViewById(R.id.add_deffence_rebound);
        final TextView showDeffenceRebound;
        showDeffenceRebound = (TextView) findViewById(R.id.show_deffence_rebound);
        final Button addObtainedBall;
        addObtainedBall = (Button) findViewById(R.id.add_obtained_ball);
        final TextView showObtainedBall;
        showObtainedBall = (TextView) findViewById(R.id.show_obtained_ball);
        final Button addLostBall;
        addLostBall = (Button) findViewById(R.id.add_lost_ball);
        final TextView showLostBall;
        showLostBall = (TextView) findViewById(R.id.show_lost_ball);
        final Button addPersonalFoul;
        addPersonalFoul = (Button) findViewById(R.id.add_personal_foul);
        final TextView showPersonalFoul;
        showPersonalFoul = (TextView) findViewById(R.id.show_personal_foul);
        final Button addObtainedFoul;
        addObtainedFoul = (Button) findViewById(R.id.add_obtained_foul);
        final TextView showObtainedFoul;
        showObtainedFoul = (TextView) findViewById(R.id.show_obtained_foul);

        /**Sharedpreferences pro ukladani dat napric apkou*/
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        /**Jednotlive promene pro data statistiky bude v DB...*/
        int P1 = pref.getInt("1P", 0);  //TH
        int P1O =pref.getInt("1PO", 0); //TH - miss
        int P2 = pref.getInt("2P", 0);  //2P
        int P2O = pref.getInt("2PO", 0);  //2P - miss
        int P3 = pref.getInt("3P", 0);  //3P
        int P3O = pref.getInt("3PO", 0);  //3P - miss
        int OR = pref.getInt("OR", 0);  //OR - utocny doskok
        int DR = pref.getInt("DR", 0);  //DR - obrany doskok
        int OB = pref.getInt("OB", 0);  //OD - získaný míč
        int LB = pref.getInt("LB", 0);  //LB - ztracený míč
        int PF = pref.getInt("PF", 0);  //PF - osobní chyba
        int OF = pref.getInt("OF", 0);  //OF - získaný faul
        showPoint.setText(String.valueOf(P1));
        showNoPoint.setText(String.valueOf(P1O));
        showPoints.setText(String.valueOf(P2));
        showNoPoints.setText(String.valueOf(P2O));
        showThreePoints.setText(String.valueOf(P3));
        showNoThreePoints.setText(String.valueOf(P3O));
        showOffenceRebound.setText(String.valueOf(OR));
        showDeffenceRebound.setText(String.valueOf(DR));
        showObtainedBall.setText(String.valueOf(OB));
        showLostBall.setText(String .valueOf(LB));
        showPersonalFoul.setText(String.valueOf(PF));
        showObtainedFoul.setText(String.valueOf(OF));

        /**Nastaveni akci na tlacitka*/
        addPoint.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P1 = pref.getInt("1P", 0);
                P1++;
                editor.putInt("1P", P1);
                editor.commit();
                showPoint.setText(String.valueOf(P1));
                finish();
            }
        });

        addNoPoint.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P1O = pref.getInt("1PO", 0);
                P1O++;
                editor.putInt("1PO", P1O);
                editor.commit();
                showNoPoint.setText(String.valueOf(P1O));
                finish();
            }
        });

        addPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P2 = pref.getInt("2P", 0);
                P2++;
                editor.putInt("2P", P2);
                editor.commit();
                showPoints.setText(String.valueOf(P2));
                finish();
            }
        });

        addNoPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P2O = pref.getInt("2PO", 0);
                P2O++;
                editor.putInt("2PO", P2O);
                editor.commit();
                showNoPoints.setText(String.valueOf(P2O));
                finish();
            }
        });

        addThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P3 = pref.getInt("3P", 0);
                P3++;
                editor.putInt("3P", P3);
                editor.commit();
                showThreePoints.setText(String.valueOf(P3));
                finish();
            }
        });

        addNoThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int P3O = pref.getInt("3PO", 0);
                P3O++;
                editor.putInt("3PO", P3O);
                editor.commit();
                showNoThreePoints.setText(String.valueOf(P3O));
                finish();
            }
        });

        addOffenceRebound.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int OR = pref.getInt("OR", 0);
                OR++;
                editor.putInt("OR", OR);
                editor.commit();
                showOffenceRebound.setText(String.valueOf(OR));
                finish();
            }
        });

        addDeffenceRebound.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int DR = pref.getInt("DR", 0);
                DR++;
                editor.putInt("DR", DR);
                editor.commit();
                showDeffenceRebound.setText(String.valueOf(DR));
                finish();
            }
        });

        addObtainedBall.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int OB = pref.getInt("OB", 0);
                OB++;
                editor.putInt("OB", OB);
                editor.commit();
                showObtainedBall.setText(String.valueOf(OB));
                finish();
            }
        });

        addLostBall.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int LB = pref.getInt("LB", 0);
                LB++;
                editor.putInt("LB", LB);
                editor.commit();
                showLostBall.setText(String.valueOf(LB));
                finish();
            }
        });

        addPersonalFoul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int PF = pref.getInt("PF", 0);
                PF++;
                editor.putInt("PF", PF);
                editor.commit();
                showPersonalFoul.setText(String.valueOf(PF));
                finish();
            }
        });

        addObtainedFoul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int OF = pref.getInt("OF", 0);
                OF++;
                editor.putInt("OF", OF);
                editor.commit();
                showObtainedFoul.setText(String.valueOf(OF));
                finish();
            }
        });
    }
}
