package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TimerActivity extends AppCompatActivity {

    Button startTimer, stopTimer, addPoints, addNoPoints, addThreePoints, addNoThreePoints, addFreeThrow, addNoFreeThrow, deleteLastAction, editLastLastAction, editLastLastLastAction, alternation, nextQuarter, finishGame;
    TextView showMinutes, showSeconds, showColon, showQuarter, showPlayer, showPlayer1, showPlayer2, showAction, showAction1, showAction2, showActionTime, showActionTime1, showActionTime2;
    CountDownTimer timer;
    long atime, gametime, min, sec, latime, llatime, lllatime;
    private static final int SET_OK = 1;
    private static int id, gameid;
    private static int quarter = 1;
    private static boolean x  = true, timeron = false;
    String teamkey, gamekey, actionkey, lastactionkey, lastlastactionkey, lastlastlastactionkey;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference actionDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        Typeface timerTypefyce = Typeface.createFromAsset(getAssets(), "segoeuil.ttf");

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id = extras.getInt("Team_ID");
            gameid = extras.getInt("Game_ID");
            atime = extras.getLong("Game_time");
            teamkey = extras.getString("teamkey");
            gamekey = extras.getString("gamekey");
            Log.d("---gamekey: ", gamekey);
            gametime = atime;
            UID = extras.getString("UserUID");
            Log.d("Reading:", "Reading team ID..." + id);
            Log.d("Reading:", "Game time-->" + atime);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na actions
        actionDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games").child(gamekey).child("actions");
        actionDatabaseReference.keepSynced(true);

        startTimer = (Button) findViewById(R.id.StartTimer);
        stopTimer = (Button) findViewById(R.id.StopTimer);
        showMinutes = (TextView) findViewById(R.id.MinutesWiew);
        showSeconds = (TextView) findViewById(R.id.SecondsWiew);
        showColon = (TextView) findViewById(R.id.ColonWiew);
        showQuarter = (TextView) findViewById(R.id.showQuarter);
        showMinutes.setTypeface(timerTypefyce);
        showSeconds.setTypeface(timerTypefyce);
        addPoints = (Button) findViewById(R.id.add_points);
        addNoPoints = (Button) findViewById(R.id.add_no_points);
        addThreePoints = (Button) findViewById(R.id.add_three_points);
        addNoThreePoints = (Button) findViewById(R.id.add_no_three_points);
        addFreeThrow = (Button) findViewById(R.id.add_point);
        addNoFreeThrow = (Button) findViewById(R.id.add_no_point);
        showPlayer = (TextView) findViewById(R.id.show_player);
        showPlayer1 = (TextView) findViewById(R.id.show_player1);
        showPlayer2 = (TextView) findViewById(R.id.show_player2);
        showAction = (TextView) findViewById(R.id.show_action);
        showAction1 = (TextView) findViewById(R.id.show_action1);
        showAction2 = (TextView) findViewById(R.id.show_action2);
        showActionTime = (TextView) findViewById(R.id.show_action_time);
        showActionTime1 = (TextView) findViewById(R.id.show_action_time1);
        showActionTime2 = (TextView) findViewById(R.id.show_action_time2);
        deleteLastAction = (Button) findViewById(R.id.delete_last_action);
        editLastLastAction = (Button) findViewById(R.id.edit_last_last_action);
        editLastLastLastAction = (Button) findViewById(R.id.edit_last_last_last_action);
        alternation = (Button) findViewById(R.id.do_alternation);
        deleteLastAction.setVisibility(View.INVISIBLE);
        editLastLastAction.setVisibility(View.INVISIBLE);
        editLastLastLastAction.setVisibility(View.INVISIBLE);
        nextQuarter = (Button) findViewById(R.id.next_quarter);
        finishGame = (Button) findViewById(R.id.end_game);

        min = (atime/(1000*60));
        sec = ((atime/1000)-min*60);
        showMinutes.setText(""+String.format("%02d",min));
        showSeconds.setText(""+String.format("%02d",sec));

        showQuarter.setText(String.valueOf(quarter));

        finishGame.setVisibility(View.INVISIBLE);
        nextQuarter.setVisibility(View.INVISIBLE);
        stopTimer.setVisibility(View.INVISIBLE);

        startTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(timeron == false){
                    startTimer.setVisibility(View.INVISIBLE);
                    stopTimer.setVisibility(View.VISIBLE);
                    startTimer();
                    timeron = true;
                }
            }
        });

        stopTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(timeron == true){
                    stopTimer.setVisibility(View.INVISIBLE);
                    startTimer.setVisibility(View.VISIBLE);
                    Log.d("----TIMER: ", "stop");
                    timer.cancel();
                    timer.cancel();
                    Log.d("----TIMER: ", "stoped");
                    timeron = false;
                }
            }
        });

        showMinutes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), SetTimerActivity.class);
                i.putExtra("Time", atime);
                i.putExtra("Timer", timeron);
                Log.d("Sending:", "Sending..." + atime);
                startActivityForResult(i, SET_OK);
                if(timeron == true){
                    timer.cancel();
                }
            }
        });

        showSeconds.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), SetTimerActivity.class);
                i.putExtra("Time", atime);
                i.putExtra("Timer", timeron);
                Log.d("Sending:", "Sending..." + atime);
                startActivityForResult(i, SET_OK);
                if(timeron == true) {
                    timer.cancel();
                }
            }
        });

        showColon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), SetTimerActivity.class);
                i.putExtra("Time", atime);
                i.putExtra("Timer", timeron);
                Log.d("Sending:", "Sending..." + atime);
                startActivityForResult(i, SET_OK);
                if(timeron == true) {
                    timer.cancel();
                }
            }
        });

        addPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_points));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        addNoPoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_no_points));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        addThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_three_points));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        addNoThreePoints.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_no_three_points));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        addFreeThrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_point));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        addNoFreeThrow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), PlayersInGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("Action", getString(R.string.add_no_point));
                i.putExtra("Time", atime);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        deleteLastAction.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //deleteLastAction();
                Intent i = new Intent(getApplicationContext(), EditActionActivity.class);
                i.putExtra("ID", gameid);
                i.putExtra("Team_ID", id);
                i.putExtra("action", showAction2.getText());
                i.putExtra("player", showPlayer2.getText());
                i.putExtra("time", latime);
                i.putExtra("actionkey", lastactionkey);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        editLastLastAction.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), EditActionActivity.class);
                i.putExtra("ID", gameid);
                i.putExtra("Team_ID", id);
                i.putExtra("action", showAction1.getText());
                i.putExtra("player", showPlayer1.getText());
                i.putExtra("time", llatime);
                i.putExtra("actionkey", lastlastactionkey);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        editLastLastLastAction.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), EditActionActivity.class);
                i.putExtra("ID", gameid);
                i.putExtra("Team_ID", id);
                i.putExtra("action", showAction.getText());
                i.putExtra("player", showPlayer.getText());
                i.putExtra("time", lllatime);
                i.putExtra("actionkey", lastlastlastactionkey);
                i.putExtra("teamkey", teamkey);
                i.putExtra("gamekey", gamekey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        alternation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AlternationActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("From_Timer", x);
                i.putExtra("teamkey", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, SET_OK);
            }
        });

        nextQuarter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                quarter++;
                atime = gametime;
                min = (atime/(1000*60));
                sec = ((atime/1000)-min*60);
                showMinutes.setText(""+String.format("%02d",min));
                showSeconds.setText(""+String.format("%02d",sec));
                showQuarter.setText(String.valueOf(quarter));
                nextQuarter.setVisibility(View.INVISIBLE);
                startTimer.setVisibility(View.VISIBLE);
                stopTimer.setVisibility(View.INVISIBLE);
            }
        });

        finishGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == SET_OK){
            if (resultCode == RESULT_OK){
                long atimepom = data.getLongExtra("SetTime", -1);
                if(atimepom != -1){
                    atime = atimepom;
                }
                Log.d("--atime: ", String.valueOf(atime));
                String la = data.getStringExtra("actionkey");
                if ((la == null) || (la.isEmpty())){

                }
                else {
                    if(lastactionkey != null && lastactionkey.equals(la)){

                    }
                    else {
                        lastlastlastactionkey = lastlastactionkey;
                        lastlastactionkey = lastactionkey;
                        lastactionkey = data.getStringExtra("actionkey");
                    }
                }
                int a = data.getIntExtra("action", -1);
                Log.d("PIIIIIAAAA", "");
                if(timeron == true && a !=1 ) {
                    Log.d("SSSSSSSSSSSS", "");
                    startTimer();
                }
                else {
                    Log.d("PIIIIIAAAA2", "222222");
                    Log.d("---ATIME???: ", String.valueOf(atime));
                    setTime(atime);
                }


                deleteLastAction.setVisibility(View.VISIBLE);
                editLastLastAction.setVisibility(View.VISIBLE);
                editLastLastLastAction.setVisibility(View.VISIBLE);

            }
        }
        showLastActions();
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showLastActions(){
        /**pro update dat v realnem case...data se zobrazuji v listu*/
        actionDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot actionDataSnapshot: dataSnapshot.getChildren()){
                    final Actions action = actionDataSnapshot.getValue(Actions.class);
                    final String actionkey = actionDataSnapshot.getKey();
                    if(actionkey.equals(lastactionkey)){
                        showPlayer2.setText(action.getPlayer());
                        showAction2.setText(action.getAction());
                        latime = action.getTime();
                        long min, sec;
                        min = (latime/(1000*60));
                        sec = ((latime/1000)-min*60);
                        showActionTime2.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
                    }
                    else if(actionkey.equals(lastlastactionkey)){
                        showPlayer1.setText(action.getPlayer());
                        showAction1.setText(action.getAction());
                        llatime = action.getTime();
                        long min, sec;
                        min = (llatime/(1000*60));
                        sec = ((llatime/1000)-min*60);
                        showActionTime1.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
                    }
                    else if(actionkey.equals(lastlastlastactionkey)){
                        showPlayer.setText(action.getPlayer());
                        showAction.setText(action.getAction());
                        lllatime = action.getTime();
                        long min, sec;
                        min = (lllatime/(1000*60));
                        sec = ((lllatime/1000)-min*60);
                        showActionTime.setText(String.format("%02d",min) + ":" + String.format("%02d",sec));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void deleteLastAction(){
        showPlayer.setText("");
        showAction.setText("");
        showActionTime.setText("");
        showPlayer1.setText("");
        showAction1.setText("");
        showActionTime1.setText("");
        showPlayer2.setText("");
        showAction2.setText("");
        showActionTime2.setText("");
        actionDatabaseReference.child(lastactionkey).removeValue();
        lastactionkey = lastlastactionkey;
        lastlastactionkey = lastlastlastactionkey;
        //lastlastlastactionkey = actionDatabaseReference.child().getKey();
        Log.d("---lllastA: ", lastlastlastactionkey);
        showLastActions();
    }

    public void startTimer(){
        timer = new CountDownTimer(atime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = (millisUntilFinished/(1000*60));
                long sec = ((millisUntilFinished/1000)-min*60);
                showMinutes.setText(""+String.format("%02d",min));
                showSeconds.setText(""+String.format("%02d",sec));
                if(millisUntilFinished < 1000){
                    showMinutes.setText("00");
                    showSeconds.setText("00");
                }
                atime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                Log.d("---timer: ", "konec?");
                if(quarter < 4){
                    timeron = false;
                    nextQuarter.setVisibility(View.VISIBLE);
                    showMinutes.setText("00");
                    showSeconds.setText("00");
                    /*showQuarter.setText(String.valueOf(quarter));
                    atime = gametime;
                    min = (atime/(1000*60));
                    sec = ((atime/1000)-min*60);
                    showMinutes.setText(""+String.format("%02d",min));
                    showSeconds.setText(""+String.format("%02d",sec));*/
                }
                else {
                    showMinutes.setText("00");
                    showSeconds.setText("00");
                    finishGame.setVisibility(View.VISIBLE);
                    timeron = false;
                    quarter = 1;
                }
                //stopTimer.setVisibility(View.INVISIBLE);
                //startTimerčřřřřtřtř.setVisibility(View.VISIBLE);
            }
        };
        timer.start();
    }

    public void setTime(long time){
        Log.d("---TimeSet: ", String.valueOf(time));
        min = (time/(1000*60));
        sec = ((time/1000)-min*60);
        showMinutes.setText(""+String.format("%02d",min));
        showSeconds.setText(""+String.format("%02d",sec));
    }
}
