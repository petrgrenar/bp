package cz.example.petr.BasketballStats;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShowTimelineActivity extends AppCompatActivity {

    private String gamekey;
    private String teamkey;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference actionDatabaseReference;
    private RecyclerView recyclerView;
    private List<Actions> actionList = new ArrayList<>();
    private ActionAdapter aAdapter;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_timeline);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            gamekey = extras.getString("gamekey");
            teamkey = extras.getString("teamkey");
            UID = extras.getString("UserUID");
            Log.d("Reading Game:", "SHOW_GAMEReading..." + gamekey);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na actions
        actionDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games").child(gamekey).child("actions");
        actionDatabaseReference.keepSynced(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        aAdapter = new ActionAdapter(actionList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(aAdapter);

        actionDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                actionList.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Actions action = noteDataSnapshot.getValue(Actions.class);
                    Log.d("----Action: ", action.getPlayer() + "-" + action.getAction());
                    actionList.add(action);
                }
                showActions();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void showActions(){
        aAdapter.notifyDataSetChanged();
        Log.d("Pocet: ", String .valueOf(aAdapter.getItemCount()));
    }
}
