package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class TeamPlayersActivity extends AppCompatActivity {

    Button p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, ok;
    private static final int ADD_OK = 1;
    private static int id_team;
    String teamkey;
    private boolean edit = true;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference playerDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_players);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            teamkey = extras.getString("teamkey");
            UID = extras.getString("UserUID");
            Log.d("Reading teamkey:", "Reading..." + teamkey);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na players
        playerDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("players");
        playerDatabaseReference.keepSynced(true);

        p1 = (Button) findViewById(R.id.player1);
        p2 = (Button) findViewById(R.id.player2);
        p3 = (Button) findViewById(R.id.player3);
        p4 = (Button) findViewById(R.id.player4);
        p5 = (Button) findViewById(R.id.player5);
        p6 = (Button) findViewById(R.id.player6);
        p7 = (Button) findViewById(R.id.player7);
        p8 = (Button) findViewById(R.id.player8);
        p9 = (Button) findViewById(R.id.player9);
        p10 = (Button) findViewById(R.id.player10);
        p11 = (Button) findViewById(R.id.player11);
        p12 = (Button) findViewById(R.id.player12);
        p13 = (Button) findViewById(R.id.player13);
        p14 = (Button) findViewById(R.id.player14);
        p15 = (Button) findViewById(R.id.player15);
        ok = (Button) findViewById(R.id.button_ok);

        p1.setText("-");
        p1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 1;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p2.setText("-");
        p2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 2;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p3.setText("-");
        p3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 3;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p4.setText("-");
        p4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 4;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p5.setText("-");
        p5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 5;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p6.setText("-");
        p6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 6;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p7.setText("-");
        p7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 7;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p8.setText("-");
        p8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 8;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p9.setText("-");
        p9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 9;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p10.setText("-");
        p10.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 10;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p11.setText("-");
        p11.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 11;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p12.setText("-");
        p12.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 12;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p13.setText("-");
        p13.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 13;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p14.setText("-");
        p14.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 14;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        p15.setText("-");
        p15.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                int b = 15;
                i.putExtra("Button", b);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });

        showPlayers();

        ok.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        showPlayers();
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void showPlayers(){
        /**pro update dat v realnem case...data se zobrazuji v listu*/
        playerDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                p1.setText("-");
                p1.setTextColor(Color.parseColor("#BFBFBF"));
                p2.setText("-");
                p2.setTextColor(Color.parseColor("#BFBFBF"));
                p3.setText("-");
                p3.setTextColor(Color.parseColor("#BFBFBF"));
                p4.setText("-");
                p4.setTextColor(Color.parseColor("#BFBFBF"));
                p5.setText("-");
                p5.setTextColor(Color.parseColor("#BFBFBF"));
                p6.setText("-");
                p6.setTextColor(Color.parseColor("#BFBFBF"));
                p7.setText("-");
                p7.setTextColor(Color.parseColor("#BFBFBF"));
                p8.setText("-");
                p8.setTextColor(Color.parseColor("#BFBFBF"));
                p9.setText("-");
                p9.setTextColor(Color.parseColor("#BFBFBF"));
                p10.setText("-");
                p10.setTextColor(Color.parseColor("#BFBFBF"));
                p11.setText("-");
                p11.setTextColor(Color.parseColor("#BFBFBF"));
                p12.setText("-");
                p12.setTextColor(Color.parseColor("#BFBFBF"));
                p13.setText("-");
                p13.setTextColor(Color.parseColor("#BFBFBF"));
                p14.setText("-");
                p14.setTextColor(Color.parseColor("#BFBFBF"));
                p15.setText("-");
                p15.setTextColor(Color.parseColor("#BFBFBF"));
                for (DataSnapshot playerDataSnapshot : dataSnapshot.getChildren()){
                    final Player player = playerDataSnapshot.getValue(Player.class);
                    final String playerkey = playerDataSnapshot.getKey();
                    if (player.getPlayerButton() == 1){
                        p1.setTextColor(Color.parseColor("#ffffff"));
                        p1.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p1.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 1;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 2){
                        p2.setTextColor(Color.parseColor("#ffffff"));
                        p2.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p2.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 2;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 3){
                        p3.setTextColor(Color.parseColor("#ffffff"));
                        p3.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p3.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 3;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 4){
                        p4.setTextColor(Color.parseColor("#ffffff"));
                        p4.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p4.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 4;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 5){
                        p5.setTextColor(Color.parseColor("#ffffff"));
                        p5.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p5.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 5;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 6){
                        p6.setTextColor(Color.parseColor("#ffffff"));
                        p6.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p6.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 6;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 7){
                        p7.setTextColor(Color.parseColor("#ffffff"));
                        p7.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p7.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 7;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 8){
                        p8.setTextColor(Color.parseColor("#ffffff"));
                        p8.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p8.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 8;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 9){
                        p9.setTextColor(Color.parseColor("#ffffff"));
                        p9.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p9.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 9;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 10){
                        p10.setTextColor(Color.parseColor("#ffffff"));
                        p10.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p10.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 10;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 11){
                        p11.setTextColor(Color.parseColor("#ffffff"));
                        p11.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p11.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 11;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 12){
                        p12.setTextColor(Color.parseColor("#ffffff"));
                        p12.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p12.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 12;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 13){
                        p13.setTextColor(Color.parseColor("#ffffff"));
                        p13.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p13.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 13;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 14){
                        p14.setTextColor(Color.parseColor("#ffffff"));
                        p14.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p14.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 14;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                    else if (player.getPlayerButton() == 15){
                        p15.setTextColor(Color.parseColor("#ffffff"));
                        p15.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p15.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                                int b = 15;
                                i.putExtra("Button", b);
                                i.putExtra("playername", player.getPlayerName());
                                i.putExtra("playersurname", player.getPlayerSurname());
                                i.putExtra("playernickname", player.getPlayerNickname());
                                i.putExtra("playernumber", player.getPlayerNumber());
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("playerkey", playerkey);
                                i.putExtra("Edit", edit);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, ADD_OK);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        /*DBHandler db = new DBHandler(this, null, null, 1);
        List<Player> players = db.getAllPlayers();
        for(final Player player : players){
            String log = "ID: " + player.getID() + ", Name: " + player.getPlayerName() + " Surname: " + player.getPlayerSurname() + " Nickname: " + player.getPlayerNickname() + " Number: " + player.getPlayerNumber() + " Team: " + player.getPlayerTeam() + " Buton: " + player.getPlayerButton() + " Ingame: " + player.getPlayerIngame();
            Log.d("Player::", log);
        }
        int b = 1;  //button, nak kterem je
        Player player = db.findPlayer(id_team, b);
        if(player != null){
            p1.setTextColor(Color.parseColor("#ffffff"));
            p1.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p1.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 1;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p1.setText("Player1");
            p1.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 1;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 2;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p2.setTextColor(Color.parseColor("#ffffff"));
            p2.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 2;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p2.setText("Player2");
            p2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 2;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 3;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p3.setTextColor(Color.parseColor("#ffffff"));
            p3.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p3.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 3;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p3.setText("Player3");
            p3.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 3;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 4;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p4.setTextColor(Color.parseColor("#ffffff"));
            p4.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p4.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 4;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p4.setText("Player4");
            p4.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 4;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 5;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p5.setTextColor(Color.parseColor("#ffffff"));
            p5.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p5.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 5;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p5.setText("Player5");
            p5.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 5;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 6;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p6.setTextColor(Color.parseColor("#ffffff"));
            p6.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p6.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 6;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p6.setText("Player6");
            p6.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 6;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 7;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p7.setTextColor(Color.parseColor("#ffffff"));
            p7.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p7.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 7;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p7.setText("Player7");
            p7.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 7;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 8;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p8.setTextColor(Color.parseColor("#ffffff"));
            p8.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p8.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 8;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p8.setText("Player8");
            p8.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 8;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 9;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p9.setTextColor(Color.parseColor("#ffffff"));
            p9.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p9.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 9;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p9.setText("Player9");
            p9.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 9;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 10;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p10.setTextColor(Color.parseColor("#ffffff"));
            p10.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p10.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 10;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p10.setText("Player10");
            p10.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 10;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 11;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p11.setTextColor(Color.parseColor("#ffffff"));
            p11.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p11.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 11;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p11.setText("Player11");
            p11.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 11;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 12;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p12.setTextColor(Color.parseColor("#ffffff"));
            p12.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p12.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 12;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p12.setText("Player12");
            p12.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 12;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 13;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p13.setTextColor(Color.parseColor("#ffffff"));
            p13.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p13.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 13;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p13.setText("Player13");
            p13.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 13;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 14;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p14.setTextColor(Color.parseColor("#ffffff"));
            p14.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p14.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 14;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p14.setText("Player14");
            p14.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 14;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        b = 15;
        player = db.findPlayer(id_team, b);
        if(player != null) {
            p15.setTextColor(Color.parseColor("#ffffff"));
            p15.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
            p15.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 15;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    i.putExtra("Edit", edit);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }
        else{
            p15.setText("Player15");
            p15.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent i = new Intent(getApplicationContext(), AddNewPlayerActivity.class);
                    int b = 15;
                    i.putExtra("Button", b);
                    i.putExtra("teamkey", teamkey);
                    Log.d("Sending teamkey: ", teamkey);
                    startActivityForResult(i, ADD_OK);
                }
            });
        }*/
    }
}
