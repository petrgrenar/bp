package cz.example.petr.BasketballStats;

/**Pro pridavani tymu*/

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddNewTeamActivity extends AppCompatActivity {

    EditText teamBox;
    Button addTeam, editTeam;
    private static int button;
    private static boolean edit = false;
    String teamName, teamkey;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private String UID;
    private DatabaseReference userDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_team);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            button = extras.getInt("Button");
            edit = extras.getBoolean("Edit");
            teamName = extras.getString("teamName");
            teamkey = extras.getString("teamkey");
            UID = extras.getString("UserUID");
            Log.d("Reading Button:", "Reading..." + button);
        }
        else {
            Log.d("Reading Button:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na users
        userDatabaseReference = basketballDatabase.getReference("users");
        userDatabaseReference.keepSynced(true);
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);

        teamBox = (EditText) findViewById(R.id.edit_team_name);
        editTeam = (Button) findViewById(R.id.edit_team);
        addTeam = (Button) findViewById(R.id.add_team);


        if(edit == false){
            editTeam.setVisibility(View.INVISIBLE);
        }
        else {
            teamBox.setText(teamName);
            editTeam.setVisibility(View.VISIBLE);
            addTeam.setVisibility(View.INVISIBLE);
        }
    }

    public void addTeam(View view){
        //Firebase add team
        final String teamId = teamDatabaseReference.push().getKey();
        Team team = new Team(teamBox.getText().toString(), button, UID);
        teamDatabaseReference.child(teamId).setValue(team);
        //userDatabaseReference.child("ss").setValue(team);
        finish();
        Log.d("Konec...", "AAAAA");

        /*mDatabase.child(teamId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Team team = dataSnapshot.getValue(Team.class);

                Log.d("Teamname: ",  team.getTeamName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }

    public void editTeam(View view){
        teamDatabaseReference.child(teamkey).child("teamName").setValue(teamBox.getText().toString());
        finish();
        Log.d("Konec...", "AAAAA");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
