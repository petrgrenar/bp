package cz.example.petr.BasketballStats;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Petr on 03.10.2016.
 * Trida pro praci s jednotlivymi tymy
 */

public class DBHandler extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "statisticsDB.db";
    private static final String TABLE_TEAMS = "teams";
    private static final String TABLE_PLAYERS = "players";
    private static final String TABLE_ACTIONS = "actions";
    private static final String TABLE_GAMES = "games";

    /**TABLE TEAMS*/
    public static final String COLUMN_TEAM_ID = "_ID_Team";
    public static final String COLUMN_TEAMNAME = "_Team_Name";
    public static final String COLUMN_TEAMBUTTON = "_Team_Button";

    /**TABLE PLAYERS*/
    public static final String COLUMN_PLAYER_ID = "_ID_Player";
    public static final String COLUMN_PLAYERNAME = "_Player_Name";
    public static final String COLUMN_PLAYERSURNAME = "_Player_Surname";
    public static final String COLUMN_PLAYERNUMBER = "_Player_Number";
    public static final String COLUMN_PLAYERNICKNAME = "_Player_Nickname";
    public static final String COLUMN_PLAYERTEAM = "_Player_Team";
    public static final String COLUMN_PLAYERBUTTON = "_Player_Button";
    public static final String COLUMN_PLAYERINGAME = "_Player_Ingame";

    /**TABLE ACTIONS*/
    public static final String COLUMN_ACTION_ID = "_ID_Action";
    public static final String COLUMN_ACTION = "_Action";
    public static final String COLUMN_PLAYER = "_Player";
    public static final String COLUMN_TIME = "_Time";
    public static final String COLUMN_GAME = "_Game";

    /**TABLE GAMES*/
    public static final String COLUMN_GAME_ID = "_ID_Game";
    public static final String COLUMN_TEAM_ONE = "_Team_One";
    public static final String COLUMN_TEAM_TWO = "_Team_Two";

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TEAMS_TABLE = "CREATE TABLE " +
                TABLE_TEAMS + "("
                + COLUMN_TEAM_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_TEAMNAME + " TEXT, "
                + COLUMN_TEAMBUTTON + " INTEGER" + ")";
        db.execSQL(CREATE_TEAMS_TABLE);
        String CREATE_PLAYERS_TABLE = "CREATE TABLE " +
                TABLE_PLAYERS + "("
                + COLUMN_PLAYER_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_PLAYERNAME + " TEXT,"
                + COLUMN_PLAYERSURNAME + " TEXT,"
                + COLUMN_PLAYERNICKNAME + " TEXT,"
                + COLUMN_PLAYERNUMBER + " INTEGER,"
                + COLUMN_PLAYERTEAM + " INTEGER, "
                + COLUMN_PLAYERBUTTON + " INTEGER, "
                + COLUMN_PLAYERINGAME + " INTEGER" + ")";
        db.execSQL(CREATE_PLAYERS_TABLE);
        String CREATE_ACTIONS_TABLE = "CREATE TABLE " +
                TABLE_ACTIONS + "("
                + COLUMN_ACTION_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_ACTION + " TEXT,"
                + COLUMN_PLAYER + " TEXT,"
                + COLUMN_TIME + " LONG,"
                + COLUMN_GAME + " INTEGER" + ")";
        db.execSQL(CREATE_ACTIONS_TABLE);
        String CREATE_GAMES_TABLE = "CREATE TABLE " +
                TABLE_GAMES + "("
                + COLUMN_GAME_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_TEAM_ONE + " TEXT,"
                + COLUMN_TEAM_TWO + " TEXT" + ")";
        db.execSQL(CREATE_GAMES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVerson, int newVersion){
        db.execSQL("DROP TABLE IF EXIST " + TABLE_TEAMS);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_PLAYERS);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_ACTIONS);
        db.execSQL("DROP TABLE IF EXIST " + TABLE_GAMES);
        onCreate(db);
    }

    /**Pridani tymu*/
    public void addTeam(Team team){
        ContentValues values = new ContentValues();
        values.put(COLUMN_TEAMNAME, team.getTeamName());
        values.put(COLUMN_TEAMBUTTON, team.getTeamButton());

        SQLiteDatabase db = this.getReadableDatabase();

        db.insert(TABLE_TEAMS, null, values);
        db.close();
    }

    /**Pridani hrace*/
    public void addPlayer(Player player){
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYERNAME, player.getPlayerName());
        values.put(COLUMN_PLAYERSURNAME, player.getPlayerSurname());
        values.put(COLUMN_PLAYERNICKNAME, player.getPlayerNickname());
        values.put(COLUMN_PLAYERNUMBER, player.getPlayerNumber());
        values.put(COLUMN_PLAYERTEAM, player.getPlayerTeam());
        values.put(COLUMN_PLAYERBUTTON, player.getPlayerButton());
        values.put(COLUMN_PLAYERINGAME, player.getPlayerIngame());

        SQLiteDatabase db = this.getReadableDatabase();

        Log.d("DB: ", String.valueOf(values));

        db.insert(TABLE_PLAYERS, null, values);
        db.close();
    }

    /**Pridani Akce*/
    public void addAction(Actions action){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ACTION, action.getAction());
        values.put(COLUMN_PLAYER, action.getPlayer());
        values.put(COLUMN_TIME, action.getTime());
        values.put(COLUMN_GAME, action.get_Game());

        SQLiteDatabase db = this.getReadableDatabase();

        db.insert(TABLE_ACTIONS, null, values);
        db.close();
    }

    /**Pridani Hry*/
    public void addGame(Game game){
        ContentValues values = new ContentValues();
        values.put(COLUMN_TEAM_ONE, game.getTeamOne());
        values.put(COLUMN_TEAM_TWO, game.getTeamTwo());

        SQLiteDatabase db = this.getReadableDatabase();

        db.insert(TABLE_GAMES, null, values);
        db.close();
    }

    /**Vyhlednai tymu*/
    public Team findTeam(int id){
        String query = "SELECT * FROM " + TABLE_TEAMS + " WHERE " + COLUMN_TEAM_ID + " = \"" + id + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Team team = new Team();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            team.setID(Integer.parseInt(cursor.getString(0)));
            team.setTeamName(cursor.getString(1));
            cursor.close();
        }else{
            team = null;
        }
        db.close();
        return team;
    }

    /**Vyhlednai hry*/
    public Game findGame(int id){
        String query = "SELECT * FROM " + TABLE_GAMES + " WHERE " + COLUMN_GAME_ID + " = \"" + id + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Game game = new Game();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            game.setID(cursor.getString(0));
            game.setTeamOne(cursor.getString(1));
            game.setTeamTwo(cursor.getString(2));
            cursor.close();
        }else{
            game = null;
        }
        db.close();
        return game;
    }

    /**Vyhlednai tymu*/
    public Team findTeamOnButton(int button){
        String query = "SELECT * FROM " + TABLE_TEAMS + " WHERE " + COLUMN_TEAMBUTTON + " = \"" + button + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Team team = new Team();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            team.setID(Integer.parseInt(cursor.getString(0)));
            team.setTeamName(cursor.getString(1));
            team.setTeamButton(cursor.getInt(2));
            cursor.close();
        }else{
            team = null;
        }
        db.close();
        return team;
    }

    /**Vyhlednai hrace*/
    public Player findPlayer(int id_team, int button){
        String query = "SELECT * FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_PLAYERTEAM + " = \"" + id_team + "\"" + " AND " + COLUMN_PLAYERBUTTON + " = \"" + button + "\"";
        //String query = "SELECT * FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_PLAYERBUTTON + " = \"" + button + "\"";
        Log.d("Reading:", "DB Query..." + query);
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Player player = new Player();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            player.setID(Integer.parseInt(cursor.getString(0)));
            player.setPlayerName(cursor.getString(1));
            player.setPlayerSurname(cursor.getString(2));
            player.setPlayerNickname(cursor.getString(3));
            player.setPlayerNumber(cursor.getInt(4));
            player.setPlayerTeam(cursor.getInt(5));
            player.setPlayerButton(cursor.getInt(6));
            player.setPlayerIngame(cursor.getInt(7));
            cursor.close();
        }else{
            player = null;
        }
        db.close();
        return player;
    }

    /**Vyhledani posledni pridane hry*/
    public Game FindLastGame(){
        String query = "SELECT * FROM " + TABLE_GAMES;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Game game = new Game();

        if(cursor.moveToLast()){
            cursor.moveToLast();
            game.setID(cursor.getString(0));
            cursor.close();
        }else{
            game = null;
        }
        db.close();
        return game;
    }

    /**Smazani vybraneho tymu*/
    public boolean deleteTeam(int id){
        boolean result = false;

        String query = "SELECT * FROM " + TABLE_TEAMS + " WHERE " + COLUMN_TEAM_ID + " = \"" + id + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Team team = new Team();

        if(cursor.moveToFirst()){
            team.setID(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_TEAMS, COLUMN_TEAM_ID + " = ?",
                    new String[] { String.valueOf(team.getID()) });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    /**Smazani vybraneho hrace*/
    public boolean deletePlayer(String playername){
        boolean result = false;

        String query = "SELECT * FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_PLAYERNAME + " = \"" + playername + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Player player = new Player();

        if(cursor.moveToFirst()){
            player.setID(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_PLAYERS, COLUMN_PLAYER_ID + " = ?",
                    new String[] { String.valueOf(player.getID()) });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    /**Smazani poslendi akce*/
    public void deleteLastAction(){
        String query = "DELETE FROM " + TABLE_ACTIONS + " WHERE " + COLUMN_ACTION_ID + " = ( SELECT MAX(" + COLUMN_ACTION_ID + ") FROM " + TABLE_ACTIONS + " )";

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    /**Pro vypis vsech tymu*/
    public List<Team> getAllTeams(){
        List<Team> teamList = new ArrayList<Team>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_TEAMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Team team = new Team();
                team.setID(Integer.parseInt(cursor.getString(0)));
                team.setTeamName(cursor.getString(1));
                //Pridani kontaktu do listu
                teamList.add(team);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return teamList;
    }

    /**Pro vypis vsech hracu*/
    public List<Player> getAllPlayers(){
        List<Player> playerList = new ArrayList<Player>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_PLAYERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Player player = new Player();
                player.setID(Integer.parseInt(cursor.getString(0)));
                player.setPlayerName(cursor.getString(1));
                player.setPlayerSurname(cursor.getString(2));
                player.setPlayerNickname(cursor.getString(3));
                player.setPlayerNumber(cursor.getInt(4));
                player.setPlayerTeam(cursor.getInt(5));
                player.setPlayerButton(cursor.getInt(6));
                player.setPlayerIngame(cursor.getInt(7));
                //Pridani kontaktu do listu
                playerList.add(player);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return playerList;
    }

    /**Pro vypis vsech hracu z urciteho tymu*/
    public List<Player> getAllTeamPlayers(int id){
        List<Player> playerList = new ArrayList<Player>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_PLAYERS + " WHERE _Player_Team =" + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Player player = new Player();
                player.setID(Integer.parseInt(cursor.getString(0)));
                player.setPlayerName(cursor.getString(1));
                player.setPlayerSurname(cursor.getString(2));
                player.setPlayerNickname(cursor.getString(3));
                player.setPlayerNumber(cursor.getShort(4));
                player.setPlayerTeam(cursor.getInt(5));
                player.setPlayerButton(cursor.getInt(6));
                player.setPlayerIngame(cursor.getInt(7));
                //Pridani kontaktu do listu
                playerList.add(player);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return playerList;
    }

    /**Pro vypis vsech akci*/
    public List<Actions> getAllActions(){
        List<Actions> actionList = new ArrayList<Actions>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_ACTIONS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Actions action = new Actions();
                action.setID(Integer.parseInt(cursor.getString(0)));
                action.setAction(cursor.getString(1));
                action.setPlayer(cursor.getString(2));
                action.setTime(cursor.getLong(3));
                action.set_Game(cursor.getInt(4));
                //Pridani kontaktu do listu
                actionList.add(action);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return actionList;
    }

    /**Pro vypis vsech akci dane hry*/
    public List<Actions> getAllGameActions(int id){
        List<Actions> actionList = new ArrayList<Actions>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_ACTIONS + " WHERE " + COLUMN_GAME + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Actions action = new Actions();
                action.setID(Integer.parseInt(cursor.getString(0)));
                action.setAction(cursor.getString(1));
                action.setPlayer(cursor.getString(2));
                action.setTime(cursor.getLong(3));
                action.set_Game(cursor.getInt(4));
                //Pridani kontaktu do listu
                actionList.add(action);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return actionList;
    }

    /**Vyhledani posledni akce*/
    public Actions findLastAction(int gameid){
        String query = "SELECT * FROM " + TABLE_ACTIONS + " WHERE " + COLUMN_GAME + " = " + gameid;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Actions action = new Actions();

        if(cursor.moveToLast()){
            cursor.moveToLast();
            action.setID(Integer.parseInt(cursor.getString(0)));
            action.setAction(cursor.getString(1));
            action.setPlayer(cursor.getString(2));
            action.setTime(cursor.getLong(3));
            cursor.close();
        }else{
            action.setAction("-");
            action.setPlayer("-");
            action.setTime(0);
        }
        db.close();
        return action;
    }

    /**Vyhledani predposledni akce*/
    public Actions findLastLastAction(int gameid){
        String query = "SELECT * FROM " + TABLE_ACTIONS + " WHERE " + COLUMN_GAME + " = " + gameid;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Actions action = new Actions();

        if(cursor.moveToPosition(cursor.getCount() - 2)){
            action.setID(Integer.parseInt(cursor.getString(0)));
            action.setAction(cursor.getString(1));
            action.setPlayer(cursor.getString(2));
            action.setTime(cursor.getLong(3));
            cursor.close();
        }else{
            action.setAction("-");
            action.setPlayer("-");
            action.setTime(0);
        }
        db.close();
        return action;
    }

    /**Vyhledani predpredposledni akce*/
    public Actions findLastLastLastAction(int gameid){
        String query = "SELECT * FROM " + TABLE_ACTIONS + " WHERE " + COLUMN_GAME + " = " + gameid;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Actions action = new Actions();

        if(cursor.moveToPosition(cursor.getCount() - 3)){
            action.setID(Integer.parseInt(cursor.getString(0)));
            action.setAction(cursor.getString(1));
            action.setPlayer(cursor.getString(2));
            action.setTime(cursor.getLong(3));
            cursor.close();
        }else{
            action.setAction("-");
            action.setPlayer("-");
            action.setTime(0);
        }
        db.close();
        return action;
    }

    /**Pro vypis vsech her*/
    public List<Game> getAllGames(){
        List<Game> gameList = new ArrayList<Game>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_GAMES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Game game = new Game();
                game.setID(cursor.getString(0));
                game.setTeamOne(cursor.getString(1));
                game.setTeamTwo(cursor.getString(2));
                //Pridani kontaktu do listu
                gameList.add(game);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return gameList;
    }

    /**Pro vypis vsech her daneho tymu*/
    public List<Game> getAllTeamGames(String team_one_name){
        List<Game> gameList = new ArrayList<Game>();
        //Vyber vseho
        String selectQuery = "SELECT * FROM " + TABLE_GAMES + " WHERE " + COLUMN_TEAM_ONE + "=\'" + team_one_name + "\'";
        Log.d("DB SELECT: ", selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //Iterovani vsemi radky
        if(cursor.moveToFirst()){
            do{
                Game game = new Game();
                game.setID(cursor.getString(0));
                game.setTeamOne(cursor.getString(1));
                game.setTeamTwo(cursor.getString(2));
                //Pridani kontaktu do listu
                gameList.add(game);
            }while (cursor.moveToNext());
        }
        //Vraceni listu
        return gameList;
    }

    /**Editace hrace ve hre na true*/
    public void editPlayerIngameTrue(Player player){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PLAYERINGAME, 1);
        Log.d("DB TRUE: ", String.valueOf(contentValues));
        db.update(TABLE_PLAYERS, contentValues, COLUMN_PLAYER_ID + " = ?",new String[] { String.valueOf(player.getID()) });
    }

    /**Editace hrace ve hre na false*/
    public void editPlayerIngameFalse(Player player){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PLAYERINGAME, 0);
        Log.d("DB FALSE: ", String.valueOf(contentValues));
        db.update(TABLE_PLAYERS, contentValues, COLUMN_PLAYER_ID + " = ?",new String[] { String.valueOf(player.getID()) });
    }

    /**Editace hrace*/
    public void editPlayer(String playername, String playersurname, String playernickname, int playernumber, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PLAYERNAME, playername);
        contentValues.put(COLUMN_PLAYERSURNAME, playersurname);
        contentValues.put(COLUMN_PLAYERNICKNAME, playernickname);
        contentValues.put(COLUMN_PLAYERNUMBER, playernumber);
        Log.d("DB EDIT PLAYER: ", String.valueOf(contentValues));
        db.update(TABLE_PLAYERS, contentValues, COLUMN_PLAYER_ID + " = ?",new String[] { String.valueOf(id) });
    }

    /**Editace tymu*/
    public void editTeam(String teamname, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_TEAMNAME, teamname);
        Log.d("DB EDIT PLAYER: ", String.valueOf(contentValues));
        db.update(TABLE_TEAMS, contentValues, COLUMN_TEAM_ID + " = ?",new String[] { String.valueOf(id) });
    }

    /**Suma hracu na hristi*/
    public int getPlayersInGame(int id_team){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT SUM(" + COLUMN_PLAYERINGAME + ") FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_PLAYERINGAME + "=\'1\' AND " + COLUMN_PLAYERTEAM + "=" + id_team, null);
        if (cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        else{
            return -1;
        }
    }

    /**Editace timeline->akce*/
    public void editAction(String player, String action, long time, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PLAYER, player);
        contentValues.put(COLUMN_ACTION, action);
        contentValues.put(COLUMN_TIME, time);
        Log.d("DB EDIT Action: ", String.valueOf(contentValues));
        Log.d("IDACTION", String.valueOf(id));
        db.update(TABLE_ACTIONS, contentValues, COLUMN_ACTION_ID + " = ?",new String[] { String.valueOf(id) });
    }
}
