package cz.example.petr.BasketballStats;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class AlternationActivity extends AppCompatActivity {

    Button p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, ok;
    private static int id_team, player_count;
    private static final int SET_OK = 1;
    private static boolean x = false;
    private static long time;
    String teamkey, gamekey;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference playerDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alternation);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id_team = extras.getInt("Team_ID");
            x = extras.getBoolean("From_Timer");
            time = extras.getLong("Game_time");
            teamkey = extras.getString("teamkey");
            gamekey = extras.getString("gamekey");
            UID = extras.getString("UserUID");
            Log.d("Reading ID_Team:", "Reading..." + id_team);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na players
        playerDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("players");
        playerDatabaseReference.keepSynced(true);

        p1 = (Button) findViewById(R.id.player1);
        p2 = (Button) findViewById(R.id.player2);
        p3 = (Button) findViewById(R.id.player3);
        p4 = (Button) findViewById(R.id.player4);
        p5 = (Button) findViewById(R.id.player5);
        p6 = (Button) findViewById(R.id.player6);
        p7 = (Button) findViewById(R.id.player7);
        p8 = (Button) findViewById(R.id.player8);
        p9 = (Button) findViewById(R.id.player9);
        p10 = (Button) findViewById(R.id.player10);
        p11 = (Button) findViewById(R.id.player11);
        p12 = (Button) findViewById(R.id.player12);
        p13 = (Button) findViewById(R.id.player13);
        p14 = (Button) findViewById(R.id.player14);
        p15 = (Button) findViewById(R.id.player15);
        ok = (Button) findViewById(R.id.button_ok);
        ok.setTextColor(Color.parseColor("#BF0906"));

        showPlayers();
    }

    public void editPlayerFalse(Player player, DBHandler db){
        db.editPlayerIngameFalse(player);
    }

    public void editPlayerTrue(Player player, DBHandler db){
        db.editPlayerIngameTrue(player);
    }

    public void showPlayers(){
        final DBHandler db = new DBHandler(this, null, null, 1);

        //player_count = db.getPlayersInGame(id_team);

        /**pro update dat v realnem case...data se zobrazuji v listu*/
        playerDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                p1.setText("-");
                p1.setTextColor(Color.parseColor("#BFBFBF"));
                p2.setText("-");
                p2.setTextColor(Color.parseColor("#BFBFBF"));
                p3.setText("-");
                p3.setTextColor(Color.parseColor("#BFBFBF"));
                p4.setText("-");
                p4.setTextColor(Color.parseColor("#BFBFBF"));
                p5.setText("-");
                p5.setTextColor(Color.parseColor("#BFBFBF"));
                p6.setText("-");
                p6.setTextColor(Color.parseColor("#BFBFBF"));
                p7.setText("-");
                p7.setTextColor(Color.parseColor("#BFBFBF"));
                p8.setText("-");
                p8.setTextColor(Color.parseColor("#BFBFBF"));
                p9.setText("-");
                p9.setTextColor(Color.parseColor("#BFBFBF"));
                p10.setText("-");
                p10.setTextColor(Color.parseColor("#BFBFBF"));
                p11.setText("-");
                p11.setTextColor(Color.parseColor("#BFBFBF"));
                p12.setText("-");
                p12.setTextColor(Color.parseColor("#BFBFBF"));
                p13.setText("-");
                p13.setTextColor(Color.parseColor("#BFBFBF"));
                p14.setText("-");
                p14.setTextColor(Color.parseColor("#BFBFBF"));
                p15.setText("-");
                p15.setTextColor(Color.parseColor("#BFBFBF"));
                player_count = 0;
                for (DataSnapshot playerDataSnapshot : dataSnapshot.getChildren()){
                    final Player player = playerDataSnapshot.getValue(Player.class);
                    final String playerkey = playerDataSnapshot.getKey();
                    if (player.getPlayerButton() == 1){
                        p1.setTextColor(Color.parseColor("#ffffff"));
                        p1.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p1.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if (player.getPlayerIngame() == 1){
                                    playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                                }
                                else{
                                    playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                                }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p1.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p1.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 2){
                        p2.setTextColor(Color.parseColor("#ffffff"));
                        p2.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p2.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p2.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p2.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 3){
                        p3.setTextColor(Color.parseColor("#ffffff"));
                        p3.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p3.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p3.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p3.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 4){
                        p4.setTextColor(Color.parseColor("#ffffff"));
                        p4.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p4.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p4.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p4.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 5){
                        p5.setTextColor(Color.parseColor("#ffffff"));
                        p5.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p5.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p5.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p5.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 6){
                        p6.setTextColor(Color.parseColor("#ffffff"));
                        p6.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p6.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p6.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p6.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 7){
                        p7.setTextColor(Color.parseColor("#ffffff"));
                        p7.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p7.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p7.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p7.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 8){
                        p8.setTextColor(Color.parseColor("#ffffff"));
                        p8.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p8.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p8.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p8.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 9){
                        p9.setTextColor(Color.parseColor("#ffffff"));
                        p9.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p9.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p9.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p9.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 10){
                        p10.setTextColor(Color.parseColor("#ffffff"));
                        p10.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p10.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p10.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p10.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 11){
                        p11.setTextColor(Color.parseColor("#ffffff"));
                        p11.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p11.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p11.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p11.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 12){
                        p12.setTextColor(Color.parseColor("#ffffff"));
                        p12.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p12.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p12.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p12.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 13){
                        p13.setTextColor(Color.parseColor("#ffffff"));
                        p13.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p13.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p13.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p13.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 14){
                        p14.setTextColor(Color.parseColor("#ffffff"));
                        p14.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p14.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p14.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p14.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                    else if (player.getPlayerButton() == 15){
                        p15.setTextColor(Color.parseColor("#ffffff"));
                        p15.setText(String.valueOf(player.getPlayerNumber()) + "\n\n" + player.getPlayerNickname());
                        p15.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                            if (player.getPlayerIngame() == 1){
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(0);
                            }
                            else{
                                playerDatabaseReference.child(playerkey).child("playerIngame").setValue(1);
                            }
                            }
                        });
                        if (player.getPlayerIngame() == 1){
                            p15.setTextColor(Color.parseColor("#06BF25"));
                            player_count++;
                        }
                        else{
                            p15.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                }
                Log.d("----player count: ", String.valueOf(player_count));
                if(player_count == 5){
                    ok.setTextColor(Color.parseColor("#06BF25"));
                    ok.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            if(x == false){
                                Intent i = new Intent(getApplicationContext(), TimerActivity.class);
                                i.putExtra("Team_ID", id_team);
                                i.putExtra("Game_time", time);
                                i.putExtra("teamkey", teamkey);
                                i.putExtra("gamekey", gamekey);
                                i.putExtra("UserUID", UID);
                                startActivityForResult(i, SET_OK);
                            }
                            finish();
                        }
                    });
                }
                else if(player_count == -1){
                    ok.setTextColor(Color.parseColor("#BF0906"));
                    ok.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, R.string.bad_players, duration);
                            toast.show();
                        }
                    });
                }
                else{
                    ok.setTextColor(Color.parseColor("#BF0906"));
                    ok.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v){
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, R.string.bad_players, duration);
                            toast.show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*List<Player> players = db.getAllPlayers();

        for(final Player player : players){
            String log = "ID: " + player.getID() + ", Name: " + player.getPlayerName() + " Surname: " + player.getPlayerSurname() + " Nickname: " + player.getPlayerNickname() + " Number: " + player.getPlayerNumber() + " Team: " + player.getPlayerTeam() + " Buton: " + player.getPlayerButton() + " Ingame: " + player.getPlayerIngame();
            Log.d("Player::", log);
        }

        int b = 1;  //button, na kterem je
        final Player player1 = db.findPlayer(id_team, b);
        if(player1 != null){
            if (player1.getPlayerIngame() == 1){
                p1.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p1.setTextColor(Color.parseColor("#ffffff"));
            }
            p1.setText(String.valueOf(player1.getPlayerNumber()) + "\n\n" + player1.getPlayerNickname());
            p1.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 1;  //button, na kterem je
                    final Player player1 = db.findPlayer(id_team, b);
                    if (player1.getPlayerIngame() == 1){
                        p1.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player1, db);
                        Log.d("Reading Player1", " if true:" + String.valueOf(player1.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p1.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player1:", " if false: "  + String.valueOf(player1.getPlayerIngame()));
                        editPlayerTrue(player1, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p1.setText("-");
        }
        b = 2;
        final Player player2 = db.findPlayer(id_team, b);
        if(player2 != null) {
            int ig = player2.getPlayerIngame();
            if (ig == 1){
                p2.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p2.setTextColor(Color.parseColor("#ffffff"));
            }
            p2.setText(String.valueOf(player2.getPlayerNumber()) + "\n\n" + player2.getPlayerNickname());
            p2.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 2;  //button, na kterem je
                    final Player player2 = db.findPlayer(id_team, b);
                    if (player2.getPlayerIngame() == 1){
                        p2.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player2, db);
                        Log.d("Reading Player2", " if true:" + String.valueOf(player2.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p2.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player2:", " if false: "  + String.valueOf(player2.getPlayerIngame()));
                        editPlayerTrue(player2, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p2.setText("-");
        }
        b = 3;
        final Player player3 = db.findPlayer(id_team, b);
        if(player3 != null) {
            if (player3.getPlayerIngame() == 1){
                p3.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p3.setTextColor(Color.parseColor("#ffffff"));
            }
            p3.setText(String.valueOf(player3.getPlayerNumber()) + "\n\n" + player3.getPlayerNickname());
            p3.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 3;  //button, na kterem je
                    final Player player3 = db.findPlayer(id_team, b);
                    if (player3.getPlayerIngame() == 1){
                        p3.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player3, db);
                        Log.d("Reading Player3", " if true:" + String.valueOf(player3.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p3.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player3:", " if false: "  + String.valueOf(player3.getPlayerIngame()));
                        editPlayerTrue(player3, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p3.setText("-");
        }
        b = 4;
        final Player player4 = db.findPlayer(id_team, b);
        if(player4 != null) {
            if (player4.getPlayerIngame() == 1){
                p4.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p4.setTextColor(Color.parseColor("#ffffff"));
            }
            p4.setText(String.valueOf(player4.getPlayerNumber()) + "\n\n" + player4.getPlayerNickname());
            p4.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 4;  //button, na kterem je
                    final Player player4 = db.findPlayer(id_team, b);
                    if (player4.getPlayerIngame() == 1){
                        p4.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player4, db);
                        Log.d("Reading Player4", " if true:" + String.valueOf(player4.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p4.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player4:", " if false: "  + String.valueOf(player4.getPlayerIngame()));
                        editPlayerTrue(player4, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p4.setText("-");
        }
        b = 5;
        final Player player5 = db.findPlayer(id_team, b);
        if(player5 != null) {
            if (player5.getPlayerIngame() == 1){
                p5.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p5.setTextColor(Color.parseColor("#ffffff"));
            }
            p5.setText(String.valueOf(player5.getPlayerNumber()) + "\n\n" + player5.getPlayerNickname());
            p5.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 5;  //button, na kterem je
                    final Player player5 = db.findPlayer(id_team, b);
                    if (player5.getPlayerIngame() == 1){
                        p5.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player5, db);
                        Log.d("Reading Player5", " if true:" + String.valueOf(player5.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p5.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player5:", " if false: "  + String.valueOf(player5.getPlayerIngame()));
                        editPlayerTrue(player5, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p5.setText("-");
        }
        b = 6;
        final Player player6 = db.findPlayer(id_team, b);
        if(player6 != null) {
            if (player6.getPlayerIngame() == 1){
                p6.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p6.setTextColor(Color.parseColor("#ffffff"));
            }
            p6.setText(String.valueOf(player6.getPlayerNumber()) + "\n\n" + player6.getPlayerNickname());
            p6.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 6;  //button, na kterem je
                    final Player player6 = db.findPlayer(id_team, b);
                    if (player6.getPlayerIngame() == 1){
                        p6.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player6, db);
                        Log.d("Reading Player6", " if true:" + String.valueOf(player6.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p6.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player6:", " if false: "  + String.valueOf(player6.getPlayerIngame()));
                        editPlayerTrue(player6, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p6.setText("-");
        }
        b = 7;
        final Player player7 = db.findPlayer(id_team, b);
        if(player7 != null) {
            if (player7.getPlayerIngame() == 1){
                p7.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p7.setTextColor(Color.parseColor("#ffffff"));
            }
            p7.setText(String.valueOf(player7.getPlayerNumber()) + "\n\n" + player7.getPlayerNickname());
            p7.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 7;  //button, na kterem je
                    final Player player7 = db.findPlayer(id_team, b);
                    if (player7.getPlayerIngame() == 1){
                        p7.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player7, db);
                        Log.d("Reading Player7", " if true:" + String.valueOf(player7.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p7.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player7:", " if false: "  + String.valueOf(player7.getPlayerIngame()));
                        editPlayerTrue(player7, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p7.setText("-");
        }
        b = 8;
        final Player player8 = db.findPlayer(id_team, b);
        if(player8 != null) {
            if (player8.getPlayerIngame() == 1){
                p8.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p8.setTextColor(Color.parseColor("#ffffff"));
            }
            p8.setText(String.valueOf(player8.getPlayerNumber()) + "\n\n" + player8.getPlayerNickname());
            p8.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 8;  //button, na kterem je
                    final Player player8 = db.findPlayer(id_team, b);
                    if (player8.getPlayerIngame() == 1){
                        p8.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player8, db);
                        Log.d("Reading Player8", " if true:" + String.valueOf(player8.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p8.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player8:", " if false: "  + String.valueOf(player8.getPlayerIngame()));
                        editPlayerTrue(player8, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p8.setText("-");
        }
        b = 9;
        final Player player9 = db.findPlayer(id_team, b);
        if(player9 != null) {
            if (player9.getPlayerIngame() == 1){
                p9.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p9.setTextColor(Color.parseColor("#ffffff"));
            }
            p9.setText(String.valueOf(player9.getPlayerNumber()) + "\n\n" + player9.getPlayerNickname());
            p9.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 9;  //button, na kterem je
                    final Player player9 = db.findPlayer(id_team, b);
                    if (player9.getPlayerIngame() == 1){
                        p9.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player9, db);
                        Log.d("Reading Player9", " if true:" + String.valueOf(player9.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p9.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player9:", " if false: "  + String.valueOf(player9.getPlayerIngame()));
                        editPlayerTrue(player9, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p9.setText("-");
        }
        b = 10;
        final Player player10 = db.findPlayer(id_team, b);
        if(player10 != null) {
            if (player10.getPlayerIngame() == 1){
                p10.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p10.setTextColor(Color.parseColor("#ffffff"));
            }
            p10.setText(String.valueOf(player10.getPlayerNumber()) + "\n\n" + player10.getPlayerNickname());
            p10.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 10;  //button, na kterem je
                    final Player player10 = db.findPlayer(id_team, b);
                    if (player10.getPlayerIngame() == 1){
                        p10.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player10, db);
                        Log.d("Reading Player10", " if true:" + String.valueOf(player10.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p10.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player10:", " if false: "  + String.valueOf(player10.getPlayerIngame()));
                        editPlayerTrue(player10, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p10.setText("-");
        }
        b = 11;
        final Player player11 = db.findPlayer(id_team, b);
        if(player11 != null) {
            if (player11.getPlayerIngame() == 1){
                p11.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p11.setTextColor(Color.parseColor("#ffffff"));
            }
            p11.setText(String.valueOf(player11.getPlayerNumber()) + "\n\n" + player11.getPlayerNickname());
            p11.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 11;  //button, na kterem je
                    final Player player11 = db.findPlayer(id_team, b);
                    if (player11.getPlayerIngame() == 1){
                        p11.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player11, db);
                        Log.d("Reading Player11", " if true:" + String.valueOf(player11.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p11.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player11:", " if false: "  + String.valueOf(player11.getPlayerIngame()));
                        editPlayerTrue(player11, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p11.setText("-");
        }
        b = 12;
        final Player player12 = db.findPlayer(id_team, b);
        if(player12 != null) {
            if (player12.getPlayerIngame() == 1){
                p12.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p12.setTextColor(Color.parseColor("#ffffff"));
            }
            p12.setText(String.valueOf(player12.getPlayerNumber()) + "\n\n" + player12.getPlayerNickname());
            p12.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 12;  //button, na kterem je
                    final Player player12 = db.findPlayer(id_team, b);
                    if (player12.getPlayerIngame() == 1){
                        p12.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player12, db);
                        Log.d("Reading Player12", " if true:" + String.valueOf(player12.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p12.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player12:", " if false: "  + String.valueOf(player12.getPlayerIngame()));
                        editPlayerTrue(player12, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p12.setText("-");
        }
        b = 13;
        final Player player13 = db.findPlayer(id_team, b);
        if(player13 != null) {
            if (player13.getPlayerIngame() == 1){
                p13.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p13.setTextColor(Color.parseColor("#ffffff"));
            }
            p13.setText(String.valueOf(player13.getPlayerNumber()) + "\n\n" + player13.getPlayerNickname());
            p13.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 13;  //button, na kterem je
                    final Player player13 = db.findPlayer(id_team, b);
                    if (player13.getPlayerIngame() == 1){
                        p13.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player13, db);
                        Log.d("Reading Player13", " if true:" + String.valueOf(player13.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p13.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player13:", " if false: "  + String.valueOf(player13.getPlayerIngame()));
                        editPlayerTrue(player13, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p13.setText("-");
        }
        b = 14;
        final Player player14 = db.findPlayer(id_team, b);
        if(player14 != null) {
            if (player14.getPlayerIngame() == 1){
                p14.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p14.setTextColor(Color.parseColor("#ffffff"));
            }
            p14.setText(String.valueOf(player14.getPlayerNumber()) + "\n\n" + player14.getPlayerNickname());
            p14.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 14;  //button, na kterem je
                    final Player player14 = db.findPlayer(id_team, b);
                    if (player14.getPlayerIngame() == 1){
                        p14.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player14, db);
                        Log.d("Reading Player14", " if true:" + String.valueOf(player14.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p14.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player14:", " if false: "  + String.valueOf(player14.getPlayerIngame()));
                        editPlayerTrue(player14, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p14.setText("-");
        }
        b = 15;
        final Player player15 = db.findPlayer(id_team, b);
        if(player15 != null) {
            if (player15.getPlayerIngame() == 1){
                p15.setTextColor(Color.parseColor("#06BF25"));
            }
            else {
                p15.setTextColor(Color.parseColor("#ffffff"));
            }
            p15.setText(String.valueOf(player15.getPlayerNumber()) + "\n\n" + player15.getPlayerNickname());
            p15.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    int b = 15;  //button, na kterem je
                    final Player player15 = db.findPlayer(id_team, b);
                    if (player15.getPlayerIngame() == 1){
                        p15.setTextColor(Color.parseColor("#ffffff"));
                        editPlayerFalse(player15, db);
                        Log.d("Reading Player15", " if true:" + String.valueOf(player15.getPlayerIngame()));
                        showPlayers();
                    }
                    else {
                        p15.setTextColor(Color.parseColor("#06BF25"));
                        Log.d("Reading Player15:", " if false: "  + String.valueOf(player15.getPlayerIngame()));
                        editPlayerTrue(player15, db);
                        showPlayers();
                    }
                }
            });
        }
        else{
            p15.setText("-");
        }*/
    }
}
