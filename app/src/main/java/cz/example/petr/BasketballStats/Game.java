package cz.example.petr.BasketballStats;

/**
 * Created by Petr on 07.11.2016.
 */

public class Game {
    private String _ID_Game;
    private String _Team_one;
    private String _Team_two;

    public Game(){

    }

    public Game(String id, String teamone, String teamtwo){
        this._ID_Game = id;
        this._Team_one = teamone;
        this._Team_two = teamtwo;
    }

    public Game(String teamone, String teamtwo){
        this._Team_one = teamone;
        this._Team_two = teamtwo;
    }

    public void setID(String id){
        this._ID_Game = id;
    }

    public String getID(){
        return this._ID_Game;
    }

    public void setTeamOne(String teamone){
        this._Team_one = teamone;
    }

    public String getTeamOne(){
        return this._Team_one;
    }

    public void setTeamTwo(String teamtwo){
        this._Team_two = teamtwo;
    }

    public String getTeamTwo(){
        return this._Team_two;
    }
}
