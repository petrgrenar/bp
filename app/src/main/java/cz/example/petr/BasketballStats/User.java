package cz.example.petr.BasketballStats;

/**
 * Created by Petr on 22.02.2017.
 */

public class User {
    private int _ID_User;
    private String _Email;
    private String _Name;

    public User(){

    }

    public User(int id, String email, String name){
        this._ID_User = id;
        this._Email = email;
        this._Name = name;
    }

    public User(String email, String name){
        this._Email = email;
        this._Name = name;
    }

    public void setID(int id){
        this._ID_User = id;
    }

    public int getID(){
        return this._ID_User;
    }

    public void setUserEmail(String email){
        this._Email = email;
    }

    public String getUserEmail(){
        return this._Email;
    }

    public void setUserName(String name){
        this._Name = name;
    }

    public String getUserName(){
        return this._Name;
    }
}
