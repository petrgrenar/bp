package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AddTeamsActivity extends AppCompatActivity {

    Button t1, t2, t3, t4, t5, t6, okbutton;
    private static final int ADD_OK = 1;
    private static boolean delete;
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private String UID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_teams);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            delete = extras.getBoolean("Delete");
            UID = extras.getString("UserUID");
        }
        else {
            Log.d("Reading:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);

        t1 = (Button) findViewById(R.id.team1);
        t2 = (Button) findViewById(R.id.team2);
        t3 = (Button) findViewById(R.id.team3);
        t4 = (Button) findViewById(R.id.team4);
        t5 = (Button) findViewById(R.id.team5);
        t6 = (Button) findViewById(R.id.team6);
        okbutton = (Button) findViewById(R.id.ok_button);

        okbutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });

        showTeams();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == ADD_OK){
            showTeams();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showTeams(){
        t1.setText("Team1");
        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 1);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        t2.setText("Team2");
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 2);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        t3.setText("Team3");
        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 3);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        t4.setText("Team4");
        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 4);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        t5.setText("Team5");
        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 5);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        t6.setText("Team6");
        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("Button", 6);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
        /**pro update dat v realnem case...data se zobrazuji v listu*/
        teamDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                for (final DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    final Team team = noteDataSnapshot.getValue(Team.class);
                    if(team.getTeamButton() == 1){
                        t1.setText(team.getTeamName());
                        t1.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                    else if(team.getTeamButton() == 2){
                        t2.setText(team.getTeamName());
                        t2.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                    else if(team.getTeamButton() == 3){
                        t3.setText(team.getTeamName());
                        t3.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                    else if(team.getTeamButton() == 4){
                        t4.setText(team.getTeamName());
                        t4.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                    else if(team.getTeamButton() == 5){
                        t5.setText(team.getTeamName());
                        t5.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                    else if(team.getTeamButton() == 6){
                        t6.setText(team.getTeamName());
                        t6.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v){
                                if(delete == true){
                                    Log.d("Mažu tým: ", team.getTeamName());
                                    teamDatabaseReference.child(noteDataSnapshot.getKey()).removeValue();
                                    finish();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*int button = 1;
        final DBHandler db = new DBHandler(this, null, null, 1);
        final Team team1 = db.findTeamOnButton(button);
        if(team1 != null){
            t1.setText(team1.getTeamName());
            if (delete == true){
                t1.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team1.getID());
                        finish();
                    }
                });
            }

        }
        else{
            if(delete == false) {
                t1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 1);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }
        button = 2;
        final Team team2 = db.findTeamOnButton(button);
        if(team2 != null){
            t2.setText(team2.getTeamName());
            if (delete == true){
                t2.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team2.getID());
                        finish();
                    }
                });
            }
        }
        else{
            if(delete == false) {
                t2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 2);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }
        button = 3;
        final Team team3 = db.findTeamOnButton(button);
        if(team3 != null){
            t3.setText(team3.getTeamName());
            if (delete == true){
                t3.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team3.getID());
                        finish();
                    }
                });
            }
        }
        else{
            if(delete == false) {
                t3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 3);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }
        button = 4;
        final Team team4 = db.findTeamOnButton(button);
        if(team4 != null){
            t4.setText(team4.getTeamName());
            if (delete == true){
                t4.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team4.getID());
                        finish();
                    }
                });
            }
        }
        else{
            if(delete == false) {
                t4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 4);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }
        button = 5;
        final Team team5 = db.findTeamOnButton(button);
        if(team5 != null){
            t5.setText(team5.getTeamName());
            if (delete == true){
                t5.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team5.getID());
                        finish();
                    }
                });
            }
        }
        else{
            if(delete == false) {
                t5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 5);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }
        button = 6;
        final Team team6 = db.findTeamOnButton(button);
        if(team6 != null){
            t6.setText(team6.getTeamName());
            if (delete == true) {
                t6.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        db.deleteTeam(team6.getID());
                        finish();
                    }
                });
            }
        }
        else{
            if(delete == false) {
                t6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                        i.putExtra("Button", 6);
                        startActivityForResult(i, ADD_OK);
                    }
                });
            }
        }*/
    }
}
