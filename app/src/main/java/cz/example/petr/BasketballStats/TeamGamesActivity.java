package cz.example.petr.BasketballStats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TeamGamesActivity extends AppCompatActivity {

    private static final int ADD_OK = 1;

    TextView matches, ateam;
    Button addGame, showGames;
    private static int id;
    String teamkey, teamname;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_games);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id = extras.getInt("Team_ID");
            teamkey = extras.getString("teamkey");
            teamname = extras.getString("teamname");
            UID = extras.getString("UserUID");
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);

        matches = (TextView) findViewById(R.id.team_matches);
        ateam = (TextView) findViewById(R.id.team);

        addGame = (Button) findViewById(R.id.add_game);
        showGames = (Button) findViewById(R.id.show_games);

        //DBHandler db = new DBHandler(this, null, null, 1);
        //Team ateam = db.findTeam(id);

        //team.setText(ateam.getTeamName());

        /**pro update dat v realnem case...data se zobrazuji v listu*/
        teamDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Log.d("NOTESNAP: ", noteDataSnapshot.getKey());
                    if (noteDataSnapshot.getKey().equals(teamkey)){
                        final Team team = noteDataSnapshot.getValue(Team.class);
                        Log.d("NejakejTym: ", team.getTeamName());
                        ateam.setText(team.getTeamName());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        matches.setText(R.string.matches);

        addGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewGameActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("teamkey", teamkey);
                i.putExtra("teamname", teamname);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });

        showGames.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), TeamGamesListActivity.class);
                i.putExtra("Team_ID", id);
                i.putExtra("teamkey", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
    }
}
