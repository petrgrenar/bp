package cz.example.petr.BasketballStats;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddNewGameActivity extends AppCompatActivity {

    Button startMatch, eightMinutes, tenMinutes;
    TextView showGameTime;
    EditText opponent;
    private static int id;
    private static final int ADD_OK = 1;
    private static boolean eight = false, ten = false;
    private static long time;
    String teamkey, teamname;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference gameDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_game);

        startMatch = (Button) findViewById(R.id.add_new_game);
        eightMinutes = (Button) findViewById(R.id.eight_minutes);
        tenMinutes = (Button) findViewById(R.id.ten_minutes);
        showGameTime = (TextView) findViewById(R.id.show_game_time);
        opponent = (EditText) findViewById(R.id.edit_opponent);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id = extras.getInt("Team_ID");
            teamkey = extras.getString("teamkey");
            teamname = extras.getString("teamname");
            UID = extras.getString("UserUID");
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na games
        gameDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("games");
        gameDatabaseReference.keepSynced(true);
    }

    public void setEight(View view){
        eight = true;
        ten = false;
        time = 480999;
        //time = 10000;
        showGameTime.setText("8m");
    }

    public void setTen(View view){
        ten = true;
        eight = false;
        time = 600999;
        showGameTime.setText("10m");
    }

    public void addGame(View view){
        Game game = new Game(teamname, opponent.getText().toString());
        final String gameId = gameDatabaseReference.push().getKey();
        gameDatabaseReference.child(gameId).setValue(game);
        Intent i = new Intent(getApplicationContext(), AlternationActivity.class);
        i.putExtra("Team_ID", id);
        i.putExtra("Game_time", time);
        i.putExtra("teamkey", teamkey);
        i.putExtra("gamekey", gameId);
        i.putExtra("UserUID", UID);
        startActivityForResult(i, ADD_OK);
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
