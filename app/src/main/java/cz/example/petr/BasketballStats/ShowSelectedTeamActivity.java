package cz.example.petr.BasketballStats;

/**Vypis vybraneho tymu a prace s nim*/

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ShowSelectedTeamActivity extends AppCompatActivity {

    private static final int ADD_OK = 1;
    private static boolean edit = true;

    TextView teamname;
    Button players, matches;
    String teamkey;

    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_selected_team);

        LinearLayout linearLayout = new LinearLayout(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            teamkey = extras.getString("teamkey");
            final TextView tw1 = new TextView(this);
            linearLayout.addView(tw1);
            UID = extras.getString("UserUID");
            Log.d("Team teamkey: ", teamkey);
        }
        else {
            Log.d("Reading Teams:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);

        teamname = (TextView) findViewById(R.id.team_name);
        players = (Button) findViewById(R.id.players);
        matches = (Button) findViewById(R.id.matches);

        /**pro update dat v realnem case...data se zobrazuji v listu*/
        teamDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Log.d("NOTESNAP: ", noteDataSnapshot.getKey());
                    if (noteDataSnapshot.getKey().equals(teamkey)){
                        final Team team = noteDataSnapshot.getValue(Team.class);
                        Log.d("NejakejTym: ", team.getTeamName());
                        teamname.setText(team.getTeamName());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        players.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), TeamPlayersActivity.class);
                i.putExtra("teamkey", teamkey);
                Log.d("Sending teamkey: ", teamkey);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });

        matches.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), TeamGamesActivity.class);
                i.putExtra("teamkey", teamkey);
                i.putExtra("teamname", teamname.getText());
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });

        teamname.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(), AddNewTeamActivity.class);
                i.putExtra("teamName", teamname.getText());
                i.putExtra("teamkey", teamkey);
                i.putExtra("Edit", edit);
                i.putExtra("UserUID", UID);
                startActivityForResult(i, ADD_OK);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == ADD_OK){
            /*DBHandler dbHandler = new DBHandler(this, null, null, 1);
            Team team = dbHandler.findTeam(id);
            teamname.setText(team.getTeamName());*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
