package cz.example.petr.BasketballStats;

/**Pro pridavani hracu*/

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddNewPlayerActivity extends AppCompatActivity {

    EditText nameBox, surnameBox, nicknameBox, numberBox;
    Button add_edit_player, delete_player;
    int id_team, button, playernumber;
    private boolean edit = false;
    String teamkey, playerkey, playername, playersurname, playernickname;
    //Firebase
    private FirebaseDatabase basketballDatabase;
    private DatabaseReference teamDatabaseReference;
    private DatabaseReference playerDatabaseReference;
    private String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_player);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id_team = extras.getInt("Team_ID");
            button = extras.getInt("Button");
            edit = extras.getBoolean("Edit");
            teamkey = extras.getString("teamkey");
            playerkey = extras.getString("playerkey");
            playername = extras.getString("playername");
            playersurname = extras.getString("playersurname");
            playernickname = extras.getString("playernickname");
            playernumber = extras.getInt("playernumber");
            UID = extras.getString("UserUID");
            Log.d("Reading:", "Reading Team ID..." + teamkey + "Button..." + button);
        }
        else {
            Log.d("Reading:", "Nothing to read...");
        }

        //Firebase
        //instance db
        basketballDatabase = FirebaseDatabase.getInstance();
        //reference na teams
        teamDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams");
        teamDatabaseReference.keepSynced(true);
        //reference na players
        playerDatabaseReference = basketballDatabase.getInstance().getReference("users").child(UID).child("teams").child(teamkey).child("players");
        playerDatabaseReference.keepSynced(true);

        nameBox = (EditText) findViewById(R.id.edit_player_name);
        surnameBox = (EditText) findViewById(R.id.edit_player_surname);
        nicknameBox = (EditText) findViewById(R.id.edit_player_nickname);
        numberBox = (EditText) findViewById(R.id.edit_player_number);
        add_edit_player = (Button) findViewById(R.id.add_player);
        delete_player = (Button) findViewById(R.id.delete_player);

        if(edit == false) {
            add_edit_player.setText(R.string.add_player);
            add_edit_player.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    addPlayer();
                }
            });
        }
        else {
            add_edit_player.setText(R.string.edit_player);
            //DBHandler dbHandler = new DBHandler(this, null, null, 1);
            //Player player = dbHandler.findPlayer(id_team, button);
            nameBox.setText(playername);
            surnameBox.setText(playersurname);
            nicknameBox.setText(playernickname);
            numberBox.setText(String.valueOf(playernumber));
            add_edit_player.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    editPlayer();
                }
            });
            delete_player.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    deletePlayer();
                }
            });
            delete_player.setVisibility(View.VISIBLE);
        }
    }

    public void addPlayer(){
        //DBHandler dbHandler = new DBHandler(this, null, null, 1);
        if(nicknameBox.getText().toString().trim().length() == 0 || numberBox.getText().toString().trim().length() == 0){
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, R.string.insert_all_data, duration);
            toast.show();
        }
        else {
            //Firebase add player
            final String playerId = playerDatabaseReference.push().getKey();
            Player player = new Player(nameBox.getText().toString(), surnameBox.getText().toString(), nicknameBox.getText().toString(), Integer.parseInt(numberBox.getText().toString()), id_team, button, 0);
            playerDatabaseReference.child(playerId).setValue(player);
            finish();
            Log.d("Konec...", "Insert Player: " + nameBox.getText().toString() + " " + surnameBox.getText().toString() + " " + Integer.parseInt(numberBox.getText().toString()) + " " + id_team + " Button= " + button);
        }
    }

    public void editPlayer(){
        DBHandler dbHandler = new DBHandler(this, null, null, 1);
        if(nicknameBox.getText().toString().trim().length() == 0 || numberBox.getText().toString().trim().length() == 0){
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, R.string.insert_all_data, duration);
            toast.show();
        }
        else {
            //Player player = dbHandler.findPlayer(id_team, button);
            //dbHandler.editPlayer(nameBox.getText().toString(), surnameBox.getText().toString(), nicknameBox.getText().toString(), Integer.parseInt(numberBox.getText().toString()), player.getID());
            playerDatabaseReference.child(playerkey).child("playerName").setValue(nameBox.getText().toString());
            playerDatabaseReference.child(playerkey).child("playerSurname").setValue(surnameBox.getText().toString());
            playerDatabaseReference.child(playerkey).child("playerNickname").setValue(nicknameBox.getText().toString());
            playerDatabaseReference.child(playerkey).child("playerNumber").setValue(Integer.parseInt(numberBox.getText().toString()));

            finish();
            Log.d("Konec...", "Update Player: " + nameBox.getText().toString() + " " + surnameBox.getText().toString() + " " + Integer.parseInt(numberBox.getText().toString()) + " " + id_team + " Button= " + button);
        }
    }

    public void deletePlayer(){
        playerDatabaseReference.child(playerkey).removeValue();
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
